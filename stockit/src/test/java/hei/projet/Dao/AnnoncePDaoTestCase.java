package hei.projet.Dao;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import hei.projet.dao.DataSourceProvider;
import hei.projet.dao.AnnoncePDao;
import hei.projet.model.AnnonceP;


public class AnnoncePDaoTestCase {
	private AnnoncePDao annoncePDao = new AnnoncePDao();
	
	@Before
	public void purgeBDD() throws Exception {
		Connection connection = DataSourceProvider.getDataSource().getConnection();
		Statement stmt = connection.createStatement();
		stmt.executeUpdate("DELETE FROM annonceP");
		stmt.executeUpdate("DELETE FROM photo");
		stmt.executeUpdate("INSERT INTO `user`(`id_user`) VALUES (1)");
		stmt.executeUpdate("INSERT INTO `annonceP`(`id_annp`,`datePubl`,`dep_loc`,`periodeLoc`,`typeEspace`,`surface`,`adresse`,`prix`,`description`,`ville`,`codePostal`,`visibilite`,`id_user`) VALUES (1,NOW(),1,'periodeLoc','cave',10,'adresse', 10,'description','ville','59000',1,1)");
		stmt.close();
		connection.close();
	}
	
	@Test
	public void testListerAnnonceP(){
		List<AnnonceP> annoncesP = annoncePDao.listerAnnonceP();
		//On vérifie les champs
		Assert.assertEquals(1, annoncesP.size());
		Assert.assertEquals(1, annoncesP.get(0).getId_annp().intValue());
		Assert.assertEquals(true, annoncesP.get(0).isDep_Loc());
		Assert.assertEquals("cave", annoncesP.get(0).getTypeEspace());
		Assert.assertEquals(10, annoncesP.get(0).getSurface().intValue());
	}
	
}
