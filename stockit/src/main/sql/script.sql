-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 12 Mai 2014 à 23:27
-- Version du serveur: 5.5.33
-- Version de PHP: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données: `stockit`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonceL`
--

CREATE TABLE `annonceL` (
  `id_annl` int(11) NOT NULL AUTO_INCREMENT,
  `datePubl` datetime DEFAULT NULL,
  `surface` int(11) DEFAULT NULL,
  `periodeLoc` varchar(12) DEFAULT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `description` text,
  `ville` varchar(30) DEFAULT NULL,
  `codePostal` varchar(5) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_annl`),
  KEY `FK_annonceL_id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `annonceL`
--

INSERT INTO `annonceL` (`id_annl`, `datePubl`, `surface`, `periodeLoc`, `dateDebut`, `dateFin`, `prix`, `description`, `ville`, `codePostal`, `id_user`) VALUES
(8, '2014-05-12 15:12:30', 12, 'hebdomadaire', '2014-05-01', '2014-05-04', 12, 'besoins de place ouech', 'Lille', '59000', 2);

-- --------------------------------------------------------

--
-- Structure de la table `annonceP`
--

CREATE TABLE `annonceP` (
  `id_annp` int(11) NOT NULL AUTO_INCREMENT,
  `datePubl` datetime DEFAULT NULL,
  `dep_loc` tinyint(1) DEFAULT NULL,
  `periodeLoc` varchar(12) DEFAULT NULL,
  `typeEspace` varchar(13) DEFAULT NULL,
  `surface` int(11) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `description` text,
  `ville` varchar(30) DEFAULT NULL,
  `codePostal` varchar(5) DEFAULT NULL,
  `visibilite` tinyint(1) DEFAULT '1',
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_annp`),
  KEY `FK_annonceP_id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `annonceP`
--

INSERT INTO `annonceP` (`id_annp`, `datePubl`, `dep_loc`, `periodeLoc`, `typeEspace`, `surface`, `adresse`, `prix`, `description`, `ville`, `codePostal`, `visibilite`, `id_user`) VALUES
(11, '2014-05-12 19:32:36', 0, 'hebdomadaire', 'Cave', 100, '88 bis rue du port', 50, 'Espace facile d''accès. Idéal pour stocker des cartons.', 'Lille', '59000', 0, 2),
(12, '2014-05-12 19:34:44', 0, 'hebdomadaire', 'Cave', 25, '80 boulevard Vauban', 20, 'Cave en sous sol, légèrement humide. Accès par ascenseur.', 'Lille', '59000', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id_message` int(11) NOT NULL AUTO_INCREMENT,
  `sujet` varchar(50) DEFAULT NULL,
  `message` text,
  `id_user` int(11) NOT NULL,
  `id_user_Association_16` int(11) NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `FK_message_id_user` (`id_user`),
  KEY `FK_message_id_user_Association_16` (`id_user_Association_16`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

CREATE TABLE `note` (
  `id_note` int(11) NOT NULL AUTO_INCREMENT,
  `note` char(1) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_user_Association_11` int(11) NOT NULL,
  PRIMARY KEY (`id_note`),
  KEY `FK_note_id_user` (`id_user`),
  KEY `FK_note_id_user_Association_11` (`id_user_Association_11`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE `photo` (
  `id_photo` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(100) DEFAULT 'fondGris.jpg',
  `id_annp` int(11) NOT NULL,
  PRIMARY KEY (`id_photo`),
  KEY `FK_photo_id_annp` (`id_annp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `photo`
--

INSERT INTO `photo` (`id_photo`, `photo`, `id_annp`) VALUES
(16, 'fondGris.jpg', 11),
(17, 'fondGris.jpg', 12);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id_reservation` int(11) NOT NULL AUTO_INCREMENT,
  `statut` varchar(30) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `contrat` varchar(50) DEFAULT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL,
  `periodePaiement` varchar(12) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `description` text,
  `id_annp` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_reservation`),
  KEY `FK_reservation_id_annp` (`id_annp`),
  KEY `FK_reservation_id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `reservation`
--

INSERT INTO `reservation` (`id_reservation`, `statut`, `contrat`, `dateDebut`, `dateFin`, `periodePaiement`, `prix`, `description`, `id_annp`, `id_user`) VALUES
(10, 'En attente de validation', '', '2014-05-13', '2014-05-14', 'hebdomadaire', 50, 'reghqethsretj', 11, 6);

-- --------------------------------------------------------

--
-- Structure de la table `typeEspace`
--

CREATE TABLE `typeEspace` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `typeEspace` varchar(13) DEFAULT NULL,
  `id_annl` int(11) NOT NULL,
  PRIMARY KEY (`id_type`),
  KEY `FK_typeEspace_id_annl` (`id_annl`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `typeEspace`
--

INSERT INTO `typeEspace` (`id_type`, `typeEspace`, `id_annl`) VALUES
(8, 'grenier', 8);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `dateNaissance` date DEFAULT NULL,
  `numeroTel` varchar(10) DEFAULT NULL,
  `visibiliteTel` tinyint(1) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `codePostal` varchar(5) DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id_user`, `nom`, `prenom`, `email`, `password`, `dateNaissance`, `numeroTel`, `visibiliteTel`, `adresse`, `ville`, `codePostal`, `photo`, `admin`) VALUES
(2, 'd''Humières', 'Tristan', 'tristan.dhumieres@hei.fr', '$2a$10$tQMJn7RKcKuol312Jx9hpeC3iRy3pGXflT4Hzbp4XOdFZDEqMSMES', '2014-05-12', '0630237677', 1, '', '', '', 'fondGris.jpg', 1),
(5, 'aze', 'aze', 'tris@hei.fr', '$2a$10$bK7/IpSSW41Jju6ULvDKZOsJEYLGCVjJ5k9./fnzGu4iq1KT2zTp6', NULL, '', 0, '', '', '', 'fondGris.jpg', 0),
(6, 'aze', 'aze', 'trist@hei.fr', '$2a$10$.wsvX.ZkciHLjvhC2UfJDubPStDKHq/CmIDi35JAA2CY6JcjonDhm', NULL, '0139588047', 1, '', '', '', 'fondGris.jpg', 0);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `annonceL`
--
ALTER TABLE `annonceL`
  ADD CONSTRAINT `FK_annonceL_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE;

--
-- Contraintes pour la table `annonceP`
--
ALTER TABLE `annonceP`
  ADD CONSTRAINT `FK_annonceP_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE;

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_message_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_message_id_user_Association_16` FOREIGN KEY (`id_user_Association_16`) REFERENCES `user` (`id_user`) ON DELETE CASCADE;

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `FK_note_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_note_id_user_Association_11` FOREIGN KEY (`id_user_Association_11`) REFERENCES `user` (`id_user`) ON DELETE CASCADE;

--
-- Contraintes pour la table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `FK_photo_id_annp` FOREIGN KEY (`id_annp`) REFERENCES `annonceP` (`id_annp`) ON DELETE CASCADE;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_reservation_id_annp` FOREIGN KEY (`id_annp`) REFERENCES `annonceP` (`id_annp`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_reservation_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE;

--
-- Contraintes pour la table `typeEspace`
--
ALTER TABLE `typeEspace`
  ADD CONSTRAINT `FK_typeEspace_id_annl` FOREIGN KEY (`id_annl`) REFERENCES `annonceL` (`id_annl`) ON DELETE CASCADE;
