<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="hei.projet.model.User"%>
<%HttpSession sessionConnecte = request.getSession(true); %>
<%User userConnecte = (User) session.getAttribute("utilisateurConnecte"); %>
<a id="titre" href="accueil"><h1>J'irai stocker chez vous</h1></a>
<a id="posterAnnonce" href="posterAnnonce">Poster votre annonce</a>
<div class="form">
	<form method="post" action="accueil">
		<input size="30" type="text" name="codeP" class="rechercher" id="recherche" placeholder="O� voulez vous stockez?"/>
		<input class="rechercher" id="submit" type="submit" value="">	
	</form>
</div>
<div class="nav1">
	<div id="nav1" <% if("accueil".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a	href="accueil">Accueil</a></div>
	<div id="nav1"<% if("annoncesLocataire".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a href="annoncesLocataire">Annonces Locataire</a></div>
	<div id="nav1"<% if("annoncesProprietaire".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a href="annoncesProprietaire">Annonces Propri�taire</a></div>
	<div id="nav1"<% if("connexion".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a href="connexion">S'inscrire/Se connecter</a></div>
	<div id="nav1"<% if("commentCaMarche".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a href="commentCaMarche">Comment �a marche?</a></div>
</div>
<c:if test="<%=(userConnecte != null)%>">
	<div class="nav2">
		<div id="nav2" <% if("monProfil".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a	href="monProfil">Mon profil</a></div>
		<div id="nav2" <% if("mesMessages".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a  href="mesMessages">Mes messages</a></div>
		<div id="nav2" <% if("mesAnnonces".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a href="mesAnnonces">Mes annonces</a></div>
		<div id="nav2" <% if("mesLocations".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a href="mesLocations">Mes r�servations</a></div>
		<div id="nav2" <% if("mesPaiments".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a href="ajouterPersonnage">Mes paiements</a></div>
	</div>
</c:if>
<c:if test="<%=(userConnecte != null && userConnecte.isAdmin())%>">
	<div class="nav3">
		<div id="nav3" <% if("listeUsersAdmin".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a	href="listeUsers">Liste des utilisateurs</a></div>
		<div id="nav3" <% if("listeAnnoncesLocataireAdmin".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a	href="listeAnnoncesLocataireAdmin">Liste des annonces locataire</a></div>
		<div id="nav3" <% if("listeAnnoncesProprietaireAdmin".equals(request.getParameter("pageSelectionnee"))) { %> class="active" <% } %>><a	href="listeAnnoncesAdmin">Liste des annonces propri�taire</a></div>
	</div>
</c:if>