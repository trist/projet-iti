<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="hei.projet.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Comment ça marche?</title>
<link rel="stylesheet" type="text/css" href="css/commentCaMarche.css">
<link rel="stylesheet" type="text/css" href="css/navigation.css">
</head>
<body>
	<jsp:include page="navigation.jsp">
		<jsp:param name="pageSelectionnee" value="commentCaMarche"/>
	</jsp:include>
	<div class="container">
		<h3>Présentation du groupe</h3>
		<p><strong>"J'irai stocker chez vous"</strong> est une plateforme de location d’espace entre particuliers. 
		Notre service met en relation des personnes souhaitant valoriser leur espace (cave, garage…) avec des personnes ayant besoin d’espace supplémentaire. Cette démarche permet de stocker près de chez vous et à moindre coût ainsi que de compléter vos revenus en tant qu’offreur d’espace.</p>
	</div>
	
	<div class="container">
		<h3>Rechercher un espace</h3>
		<p><strong>Créez gratuitement vos annonces</strong></br>

Suivez le guide et renseignez les caractéristiques et photos de vos annonces.
N’hésitez pas à nous contacter pour de l’aide ou pour tout renseignement.
Votre annonce est maintenant disponible sur notre site.

</br><strong>Trouvez des locataires potentiels</strong></br>


Vos locataires potentiels effectuent des demandes de location, celles-ci s’affichent dans votre profil.
Notre module de recherche vous permet également de trouver vous-même les locataires qui vous correspondent. 

</br><strong>Echangez et choisissez vos locataires</strong></br>


Répondez aux questions de vos locataires à l’aide de notre messagerie sécurisée. Vos coordonnées sont protégées et vous les échangez uniquement lorsque vous le souhaitez.
Vous validez la demande de location selon votre choix. 

</br><strong>La location peut commencer</strong></br>


Une fois la location validée, vous fixez un rendez-vous.
Vous accueillez les biens de votre locataire et lui remettez les clés ou non s’il s’agit d’une location exclusive  ou d'un dépôt .


</p>
	</div>
	
	<div class="container">
		<h3>Louer votre espace</h3>
		<p><strong>Renseignez votre besoin</strong></br>

					Suivez le guide et définissez dans une annonce les critères de votre besoin d'espace de stockage. Cela permettra aux propriétaires d’espaces adéquats de vous contacter. Vous pouvez également directement faire une recherche parmi les espaces disponibles sur le site (étape suivante).
					
					</br><strong>Trouvez un espace adéquat</strong></br>


					Renseignez vos critères sur notre module de recherche pour trouver les espaces correspondants à vos attentes (localisation, superficie …). Puis faites vos demandes de location directement auprès du propriétaire.


					</br><strong>Echangez avec le propriétaire et réservez</strong></br>

					Posez toutes vos questions (conditions d’accès, fréquence des visites…) aux propriétaires à l’aide de notre messagerie sécurisée. Vos coordonnées sont protégées et vous les échangez uniquement lorsque vous le souhaitez.

					</br><strong>La location peut commencer</strong></br>


					Une fois la location validée, vous fixez un rendez-vous.
					Vous pouvez donc déposer vos affaires et récupérer les clés ou non s’il s’agit d’une location exclusive  ou d'un dépôt .


</p>
	</div>
</body>
</html>
    