<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="hei.projet.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>J'irai stocker chez vous</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/accueil.css" rel="stylesheet">
		<link href="css/navigation.css" rel="stylesheet">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
	    <script src="js/jquery.vmap.js" type="text/javascript"></script>
	    <script src="js/jquery.vmap.france.js" type="text/javascript"></script>
		<script src="js/jquery.vmap.colorsFrance.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			$('#francemap').vectorMap({
			    map: 'france_fr',
				hoverOpacity: 0.5,
				hoverColor: false,
				backgroundColor: "#ffffff",
				colors: couleurs,
				borderColor: "#000000",
				selectedColor: "#EC0000",
				enableZoom: true,
				showTooltip: true,
			    onRegionClick: function(element, code, region)
			    {
			    	window.location.href="annoncesProprietaire?codP="+code+"000"+"&depLoc=&surf=&check1=&check2=&check3=&check4=";
			        
			    }
			});
		});
		</script>
	</head>
	<body>
		<jsp:include page="navigation.jsp">
		    <jsp:param name="pageSelectionnee" value="accueil"/>
		</jsp:include>
		<div class="accueil">
			<div id="francemap"></div>
			<div class="col2">
				<section>
					<img class="icone" src="img/icon_proche.ico" alt="icon_proche.ico" title="Proche">
					<span id="proche">
						<h3>Proche</h3>
						<p>Trouvez près de chez vous l'espace qui vous convient.</p>
					</span>
				</section>
				<section>
					<img class="icone" src="img/icon_economique.ico" alt="icon_economique.ico" title="Economique">
					<span id="economique">
						<h3>Economique</h3>
						<p>Inscription et utilisation gratuite du site.</br>Propriétaire, gagner de l'argent en rentabilisant votre espace inutilisé.</br>Locataire, trouvez la meilleure offre selon votre besoin et votre budget.</p>
					</span>
				</section>
				<section>
					<img class="icone" src="img/icon_complet.ico" alt="icon_complet.ico" title="Complet">
					<span id="complet">
						<h3>Complet</h3>
						<p>Pour un besoin ponctuel ou une location à lannée, comparez et réservez facilement en ligne.</br>Rejoignez une communauté conviviale.</p>
					</span>
				</section>
			</div>
		</div>	
	</body>
</html>