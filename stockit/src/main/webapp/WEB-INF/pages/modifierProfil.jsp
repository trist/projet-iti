<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="hei.projet.model.User"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Mon profil</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/navigation.css" rel="stylesheet">
		<link href="css/modifierProfil.css" rel="stylesheet">
	</head>
	<body>
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="monProfil"/>
		</jsp:include>
		<div class="modifierProfil">
			<h3>Modifier votre profil :</h3>
			<div class="formulaireModifierProfil">
				<form class="formulaireProfil" action="modifierProfil" method="post" enctype="multipart/form-data">
					<label for="nom" class="texte">Nom : </label>
					<input type="text" class="form-control" size="30" name="nom" id="nom" value="${userInformations.nom}"/></br>
					<label for="prenom" class="texte">Prenom : </label>
					<input type="text" class="form-control" size="30" name="prenom" id="prenom" value="${userInformations.prenom}"/></br>
					<label for="mail" class="texte">E-mail : </label>
					<input type="text" class="form-control" size="30" name="mail" id="mail" value="${userInformations.mail}"/></br>
					<label for="rue" class="texte">Adresse : </label>
					<input type="text" class="form-control" size="45" name="adresse" id="adresse" value="${userInformations.adresse}"/></br>
					<label for="ville" class="texte">Ville : </label>
					<input type="text" class="form-control" size="30" name="ville" id="ville" value="${userInformations.ville}"/></br>
					<label for="codepostal" class="texte">Code Postal : </label>
					<input type="text" class="form-control" name="codePostal" id="codepostal" value="${userInformations.codePostal}"/></br>
					<label  for="dateNaissance" class="texte">Date de naissance : </label>
					<input type="date" class="form-control" name="dateNaissance" id="dateNaissance" placeholder="aaaa-mm-jj"/></br>
					<label for="numTel" class="texte">Téléphone : </label>
					<input type="text" class="form-control" size="12" maxlength="10" name="numTel" id="numTel" value="${userInformations.numTel}"/></br>
					<label for="visibiliteTel" class="texte">Téléphone visible : </label>
					<input type= "radio" name="visibiliteTel" value="true"> Oui
					<input type= "radio" name="visibiliteTel" value="false" checked> Non</br>
					<label for="fichier">Photo de profil : </label>
	                <input type="file" id="fichier" name="fichier"  value="${userInformations.photo}"/><span class="erreur"><c:out value="${fichier}" /></span></br></br>
					<input class="valider" type="submit" value="Valider">
				</form>
			</div>
		</div>
	</body>
</html>