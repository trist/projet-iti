<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="hei.projet.model.User"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Mon profil</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/navigation.css" rel="stylesheet">
		<link href="css/monProfil.css" rel="stylesheet">
	</head>
	<body>
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="monProfil"/>
		</jsp:include>
		<div class="profil">
			<section class="details">
				<p class="first-line">Nom : ${userInformations.nom}</p>
				<p>Prénom : ${userInformations.prenom}</p>
				<p>E-mail:  ${userInformations.mail}</p>
				<p>Adresse : ${userInformations.adresse}</p>
				<p>Ville : ${userInformations.ville}</p>
				<p>CP: ${userInformations.codePostal}</p>
				<%User user = (User) request.getAttribute("userInformations"); %>
				<p>Date de naissance : <c:if test="<%=(user.getDateNaissance() != null)%>"><fmt:formatDate value="${userInformations.dateNaissance}" pattern="dd MMMM yyyy"/></c:if></p>
				<p>Téléphone : ${userInformations.numTel}</p>
				<p>Note : ${note}/5</p>
			</section>
			<a id="modifierProfil" href="modifierProfil">Modifier mon profil</a>
			<img class="photo" src="img/${userInformations.photo}" alt="${userInformations.photo}" title="Ma photo">	
		</div>
	</body>
</html>