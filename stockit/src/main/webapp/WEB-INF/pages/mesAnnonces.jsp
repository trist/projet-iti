<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="hei.projet.model.User"%>
<%@page import="hei.projet.model.Photo"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Mes annonces</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/accueil.css" rel="stylesheet">
		<link href="css/navigation.css" rel="stylesheet">
		<link href="css/mesAnnonces.css" rel="stylesheet">
		<script src="js/mesLocations.js" type="text/javascript"></script>
	</head>
	<body>
		<jsp:include page="navigation.jsp">
	    	<jsp:param name="pageSelectionnee" value="mesAnnonces"/>
		</jsp:include>
		<div class="mesAnnonces">
			<h2>Vous proposez :</h2>
					<%List<Photo> photos = (List) request.getAttribute("listePhoto"); %>
					<%int j=0; %>
			<c:forEach var="annonceP" items="${annoncesP}">
			<div class="annonce">
				<div class="col" id="col_1">
					<img id="imgAnnonce" src="img/<%=photos.get(j).getPhoto()%>" alt="<%=photos.get(j).getPhoto()%>">
					<%j++; %>
				</div>
				<div class="col" id="col_2">
					<h4>${annonceP.typeEspace} de ${annonceP.surface}m2</h4>
					<p>${annonceP.ville}</br>${annonceP.codePostal}</br>${annonceP.adresse}</br>${annonceP.surface}m2</br>${annonceP.prix}€ ${annonceP.periodeLocation}</br></p>
				</div>
				<div class="col" id="col_3">
					<c:if test="${not annonceP.dep_Loc}">Depot</c:if><c:if test="${annonceP.dep_Loc}">Location</c:if><p></br>Disponible maintenant</p>
				</div>
				<span style="cursor: pointer;" class="modifier" onclick='window.location.href="modifierAnnonceProprietaire?idAnnP=${annonceP.id_annp}";'>Modifier</span>
				<span style="cursor: pointer;" class="supprimer" onclick="confirmationSuppressionAnnonceP(${annonceP.id_annp});">Supprimer</span>
			</div>
			</c:forEach>
			<h2>Vous recherchez :</h2>
			<c:forEach var="annonceL" items="${annoncesL}">
			<div class="annonce">
				<div class="col" id="col_1">
					<h4>Espace de ${annonceL.surface}m2</h4>
					<p>${annonceL.ville}</br>${annonceL.codePostal}</br>${annonceL.surface}m2</br>${annonceL.prix}€ ${annonceL.periodeLoc}</br></p>
				</div>
				<div class="col" id="col_3">
					<p>Actuellement en recherche</p>
				</div>
				<span style="cursor: pointer;" class="modifier" onclick='window.location.href="modifierAnnonceLocataire?idAnnL=${annonceL.id_annl}";'>Modifier</span>
				<span style="cursor: pointer;" class="supprimer" onclick="confirmationSuppressionAnnonceL(${annonceL.id_annl});">Supprimer</span>
			</div>
			</c:forEach>
			<a class="creerAnnonce" href="posterAnnonce">Cr&eacuteer une nouvelle annonce</a>
		</div>
	</body>
</html>
