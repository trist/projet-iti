<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="hei.projet.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	
		<title>Liste des utilisateurs</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/navigation.css" rel="stylesheet">
		<link href="css/listeUsers.css" rel="stylesheet">
	</head>
	<body>
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="listeUsersAdmin"/>
		</jsp:include>
		<div class="listeUsers">
			<div class="tableau">
				<h3>Utilisateurs</h3>
				<table>
					<thead>
						<th class="col1">Nom</th>
						<th class="col2">Prénom</th>
						<th class="col3">E-mail</th>
						<th class="col4">Promouvoir</th>
						<th class="col5">Supprimer</th>
					</thead>
					<tbody>
						<c:forEach var="user" items="${listeUser}">
							<tr>
								<td>${user.nom}</td>
								<td>${user.prenom }</td>
								<td>${user.mail}</td>
								<td><a href="promouvoirUserAdmin?idUser=${user.id_user}"><img id="detail" src="img/promouvoirIcon.jpg" alt="promouvoirIcon.jpg" title="Promouvoir admin"></a></td>
								<td><a href="supprimerUser?idUser=${user.id_user}"><img id="detail" src="img/supprimer.png" alt="supprimer.png" title="Supprimer user"></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<h3>Administrateur</h3>
				<table>
					<thead>
						<th class="col1">Nom</th>
						<th class="col2">Prénom</th>
						<th class="col3">E-mail</th>
						<th class="col4">Dégrader</th>
						<th class="col5">Supprimer</th>
					</thead>
					<tbody>
						<c:forEach var="user" items="${listeUserAdmin}">
							<tr>
								<td>${user.nom}</td>
								<td>${user.prenom }</td>
								<td>${user.mail}</td>
								<td><a href="degraderUser?idUser=${user.id_user}"><img id="detail" src="img/degraderIcon.png" alt="degraderIcon.png" title="Dégrader admin"></a></td>
								<td><a href="supprimerUser?idUser=${user.id_user}"><img id="detail" src="img/supprimer.png" alt="supprimer.png" title="Supprimer user"></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>	
		</div>
	</body>
</html>