<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modifier annonce propriétaire</title>
<link href="css/navigation.css" rel="stylesheet">
<link href="css/posterAnnonce.css" rel="stylesheet">
</head>
<body>
	<jsp:include page="navigation.jsp">
		<jsp:param name="pageSelectionnee" value="mesAnnonces"/>
	</jsp:include>
	<div class="posterAnnonce">
		<form action="modifierAnnonceProprietaire" method="post" enctype="multipart/form-data">
			<div class="formulaire" id="formulaireProprietaire">
				<div>
					<label for="codepostal" class="texte">Code Postal : </label>
					<input type="text" class="form-control" name="codePostal" id="codepostal" value="${annonceP.codePostal}"/>
				</div>
				<div>
					<label for="ville" class="texte">Ville : </label>
					<input type="text" class="form-control" size="30" name="ville" id="ville" value="${annonceP.ville}"/>
				</div>
				<div>
					<label for="adresse" class="texte">Adresse : </label>
					<input type="text" class="form-control" size="45" name="adresse" id="adresse" value="${annonceP.ville}"/>
				</div>
				<div>
					<label for="surface" class="texte">Surface : </label>
					<input type="text" class="form-control" name="surface" id="surface" value="${annonceP.surface}"/>m2<span class="erreur"><c:out value="${surface}" /></span>
				</div>
				<div>
					<label for="type" class="texte">Type de contrat : </label>
					<select class="typeContrat" name="typeContrat">
						<option name="typeContrat" id="type" value="False">Dépot</option>
						<option name="typeContrat" id="type" value="True">Location</option>
					</select>
				</div>
				<div>
					<label for="type" class="texte">Période de location : </label>
					<select class="periodeLocation" name="periodeLocation" id="periodeLocation" onChange="afficherPrix();">
						<option id="type" value="hebdomadaire">Hebdomadaire</option>
						<option id="type" value="mensuel">Mensuelle</option>
					</select>
				</div>
				<div style="display:" id="hebdomadaire">
					<label for="prixHebdomadaire" class="texte">Prix par semaine : </label>
					<input type="text" class="form-control" name="prixHebdomadaire" id="prix"/>€/semaine<span class="erreur"><c:out value="${prixHebdomadaire}"/></span>
				</div>
				<div style="display:none" id="mensuel">
					<label for="prixMensuel" class="texte">Prix par mois : </label>
					<input type="text" class="form-control" name="prixMensuel" id="prix"/>€/mois<span class="erreur" value="${annonceP.prix}"><c:out value="${prixMensuel}"/></span>
				</div>
				<div>
					<label for="typeEspace" class="texte">Type d'espace : </label>
					<select class="form-control" name="typeEspace" id="type">
						<option id="type" value="Cave">Cave</option>
						<option id="type" value="Grenier">Grenier</option>
						<option id="type" value="Garage">Garage</option>
						<option id="type" value="Pièce a vivre">Pièce à vivre</option>
					</select>
				</div>
				<div>
					<label for="description" class="texte">Description : </label>
					<textarea rows="5" cols="35" class="form-control" name="description" id="description">${annonceP.description}</textarea>
				</div>
				<p>Ajoutez de une à quatres photos (les formats autorisés sont .gif, .png et .jpg) :</p>
				<div>
					<label for="fichier">Photo n°1 : </label>
                	<input type="file" id="fichier" name="fichier1" /><span class="erreur"><c:out value="${fichier1}" /></span>
				</div>
				<div>
					<label for="fichier">Photo n°2 : </label>
                	<input type="file" id="fichier" name="fichier2" /><span class="erreur"><c:out value="${fichier2}" /></span>
				</div>
				<div>
					<label for="fichier">Photo n°3 : </label>
                	<input type="file" id="fichier" name="fichier3" /><span class="erreur"><c:out value="${fichier3}" /></span>
				</div>
				<div>
					<label for="fichier">Photo n°4 : </label>
                	<input type="file" id="fichier" name="fichier4" /><span class="erreur"><c:out value="${fichier4}" /></span>
				</div>
				<div class="champ2">
					<input class="valider" type="submit" value="Valider">
				</div>
			</div>
		</form>
	</div>
</body>
</html>