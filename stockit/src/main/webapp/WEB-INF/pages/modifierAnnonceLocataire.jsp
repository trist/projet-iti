<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modifier annonce propriétaire</title>
<link href="css/navigation.css" rel="stylesheet">
<link href="css/posterAnnonce.css" rel="stylesheet">
</head>
<body>
	<jsp:include page="navigation.jsp">
		<jsp:param name="pageSelectionnee" value="mesAnnonces"/>
	</jsp:include>
	<div class="posterAnnonce">
		<form action="modifierAnnonceLocataire" method="post">
			<div class="formulaire" id="formulaireLocataire">
				<div>
					<label for="codePostalL" class="texte">Code Postal : </label>
					<input type="text" class="form-control" name="codePostalL" id="codepostalL" value="${annonceL.codePostal}"/>
				</div>
				<div>
					<label for="villeL" class="texte">Ville : </label>
					<input type="text" class="form-control" size="30" name="villeL" id="villeL" value="${annonceL.ville}"/>
				</div>
				<div>
					<label for="surfaceL" class="texte">Surface : </label>
					<input type="text" class="form-control" name="surfaceL" id="surfaceL" value="${annonceL.surface}"/>m2
				</div>
				<div>
					<label for="periodeLocationL" class="texte">Période de location : </label>
					<select class="periodeLocation" name="periodeLocationL" id="periodeLocationL" onChange="afficherPrixL();">
						<option id="periodeLocationL" value="hebdomadaire">Hebdomadaire</option>
						<option id="periodeLocationL" value="mensuel">Mensuelle</option>
					</select>
				</div>
				<div style="display:" id="hebdomadaireL">
					<label for="prixHebdomadaire" class="texte">Prix par semaine : </label>
					<input type="text" class="form-control" name="prixHebdomadaireL" id="prix"/>€/semaine<c:out value="${prixHebdomadaire}" />
				</div>
				<div style="display:none" id="mensuelL">
					<label for="prixMensuel" class="texte">Prix par mois : </label>
					<input type="text" class="form-control" name="prixMensuelL" id="prix"/>€/mois<c:out value="${prixMensuel}" />
				</div>
				<div>
					<label  for="datedebut" class="texte">Date de début : </label>
					<input type="date" class="form-control" name="datedebut" id="datedebut" placeholder="aaaa-mm-jj"/>
				</div>
				<div>
					<label  for="date" class="texte">Date de fin : </label>
					<input type="date" class="form-control" name="datefin" id="datefin" placeholder="aaaa-mm-jj"/>
				</div>
				<div>
					<label for="typeEspaceL">Type(s) d'espace : </label>
					<input type="checkbox" name="checkbox1" value="cave">Cave
					<input type="checkbox" name="checkbox2" value="grenier">Grenier
					<input type="checkbox" name="checkbox3" value="garage">Garage
					<input type="checkbox" name="checkbox4" value="pieceVivre">Pièce à vivre
				</div>
				<div>
					<label for="descriptionL" class="texte">Description : </label>
					<textarea rows="5" cols="35" type="textarea" class="form-control" name="descriptionL" id="description" value="${annonceL.description}"></textarea>
				</div>
				<div class="champ2">
					<input class="valider" type="submit" value="Valider">
				</div>
			</div>
		</form>
	</div>
</body>
</html>