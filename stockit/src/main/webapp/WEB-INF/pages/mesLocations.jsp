<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="hei.projet.model.Reservation"%>
<%@page import="hei.projet.model.AnnonceP"%>
<%@page import="hei.projet.model.User"%>
<%@page import="hei.projet.model.Photo"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Mes locations</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/navigation.css" rel="stylesheet">
		<link href="css/mesLocations.css" rel="stylesheet">
		<script src="js/mesLocations.js" type="text/javascript"></script>
	</head>
	<body>
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="mesLocations"/>
		</jsp:include>
		<%List<Reservation> listeReservation = (List) request.getAttribute("listeReservation"); %>
		<%List<AnnonceP> listeAnnonceP = (List) request.getAttribute("listeAnnonceP"); %>
		<%List<Photo> listePhoto = (List) request.getAttribute("listePhoto"); %>
		<%List<User> listeAnnonceur = (List) request.getAttribute("listeAnnonceur"); %>
		<%int j=0; %>
		<%String statut="En attente de validation"; %>
		<div class="mesLocations">
		<h2>Suivez le statut de vos réservations :</h2>
		<c:if test="<%=(listeReservation.size()==0) %>"><h3>Vous n'avez aucune réservation en cours.</h3></c:if>
		<c:if test="<%=(listeReservation.size()!=0) %>">
			<c:forEach var="annonceP" items="${listeAnnonceP}">
				<c:if test="<%=(listeReservation.get(j).getStatut().compareTo(statut)==0) %>">
					<div class="reservation">
						<div class="col" id="col_1">
							<img id="imgAnnonce" src="img/<%=listePhoto.get(j).getPhoto()%>" alt="<%=listePhoto.get(j).getPhoto()%>">
						</div>
						<div class="col" id="col_2">
							<h4>${annonceP.typeEspace} de ${annonceP.surface}m2</h4>
							<p>${annonceP.ville}</br>${annonceP.codePostal}</br>${annonceP.adresse}</br>${annonceP.surface}m2</br>${annonceP.prix}€/${annonceP.periodeLocation}</br></p>
						</div>
						<div class="col" id="col_3">
							<p>Statut : En attente de validation</br>Type de contrat :<c:if test="${not annonceP.dep_Loc}"> Dépot</c:if><c:if test="${annonceP.dep_Loc}">Location</c:if> </br>Du <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservation.get(j).getDateDebut() %>" /> au <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservation.get(j).getDateFin() %>" /></br>Propriétaire : <%=listeAnnonceur.get(j).getPrenom() %> <%=listeAnnonceur.get(j).getNom() %></p>
						</div>
						<span style="cursor: pointer;" class="bouton" id="details_1" onclick='window.location.href="detailsReservation?idReservation=<%=listeReservation.get(j).getId_reserv()%>";'>Détails</span>
					</div>
				</c:if>
				<%j++; %>
			</c:forEach>
			<%j=0; %>
			<%statut="En attente de paiement"; %>
			<c:forEach var="annonceP" items="${listeAnnonceP}">
				<c:if test="<%=(listeReservation.get(j).getStatut().compareTo(statut)==0) %>">
					<div class="reservation">
						<div class="col" id="col_1">
							<img id="imgAnnonce" src="img/<%=listePhoto.get(j).getPhoto()%>" alt="<%=listePhoto.get(j).getPhoto()%>">
						</div>
						<div class="col" id="col_2">
							<h4>${annonceP.typeEspace} de ${annonceP.surface}m2</h4>
							<p>${annonceP.ville}</br>${annonceP.codePostal}</br>${annonceP.adresse}</br>${annonceP.surface}m2</br>${annonceP.prix}€/${annonceP.periodeLocation}</br></p>
						</div>
						<div class="col" id="col_3">
							<p>Statut : En attente de paiement</br>Type de contrat :<c:if test="${not annonceP.dep_Loc}"> Dépot</c:if><c:if test="${annonceP.dep_Loc}">Location</c:if> </br>Du <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservation.get(j).getDateDebut() %>" /> au <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservation.get(j).getDateFin() %>" /></br>Propriétaire : <%=listeAnnonceur.get(j).getPrenom() %> <%=listeAnnonceur.get(j).getNom() %></p>
						</div>
						<span style="cursor: pointer;" class="bouton" id="payer" onclick="confirmationPaiement(<%=listeReservation.get(j).getId_reserv()%>);">Payer</span>
						<span style="cursor: pointer;" class="bouton" id="annuler" onclick="confirmationAnnulation(<%=listeReservation.get(j).getId_reserv()%>);">Annuler</span>
						<span style="cursor: pointer;" class="bouton" id="details_2" onclick='window.location.href="detailsReservation?idReservation=<%=listeReservation.get(j).getId_reserv()%>";'>Détails</span>
					</div>
				</c:if>
				<%j++; %>
			</c:forEach>
			<%j=0; %>
			<%statut="Validee"; %>
			<c:forEach var="annonceP" items="${listeAnnonceP}">
				<c:if test="<%=(listeReservation.get(j).getStatut().compareTo(statut)==0) %>">
					<div class="reservation">
						<div class="col" id="col_1">
							<img id="imgAnnonce" src="img/<%=listePhoto.get(j).getPhoto()%>" alt="<%=listePhoto.get(j).getPhoto()%>">
						</div>
						<div class="col" id="col_2">
							<h4>${annonceP.typeEspace} de ${annonceP.surface}m2</h4>
							<p>${annonceP.ville}</br>${annonceP.codePostal}</br>${annonceP.adresse}</br>${annonceP.surface}m2</br>${annonceP.prix}€/${annonceP.periodeLocation}</br></p>
						</div>
						<div class="col" id="col_3">
							<p>Statut : Validée</br>Type de contrat :<c:if test="${not annonceP.dep_Loc}"> Dépot</c:if><c:if test="${annonceP.dep_Loc}">Location</c:if> </br>Du <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservation.get(j).getDateDebut() %>" /> au <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservation.get(j).getDateFin() %>" /></br>Propriétaire : <%=listeAnnonceur.get(j).getPrenom() %> <%=listeAnnonceur.get(j).getNom() %></p>
						</div>
						<span style="cursor: pointer;" class="bouton" id="details_1" onclick='window.location.href="attribuerNote?idUserNote=<%=listeAnnonceur.get(j).getId_user()%>";'>Noter</span>
						<span style="cursor: pointer;" class="bouton" id="details_1" onclick='window.location.href="detailsReservation?idReservation=<%=listeReservation.get(j).getId_reserv()%>";'>Détails</span>
					</div>
				</c:if>
				<%j++; %>
			</c:forEach>
		</c:if>
		<%List<Reservation> listeReservationEspacePropose = (List) request.getAttribute("listeReservationEspacePropose"); %>
		<%List<AnnonceP> listeAnnoncePropose = (List) request.getAttribute("listeAnnoncePropose"); %>
		<%List<Photo> listePhotoAnnoncePropose = (List) request.getAttribute("listePhotoAnnoncePropose"); %>
		<%List<User> listeLocataire = (List) request.getAttribute("listeLocataire"); %>
		<%j=0; %>
		<%statut="En attente de validation"; %>
		<h2>Suivez le statut des réservations sur vos espaces à louer :</h2>
		<c:if test="<%=(listeReservationEspacePropose.size()==0) %>"><h3>Il n'y a aucune réservation sur vos espaces à louer.</h3></c:if>
		<c:if test="<%=(listeReservationEspacePropose.size()!=0) %>">
			<c:forEach var="annoncePropose" items="${listeAnnoncePropose}">
				<c:if test="<%=(listeReservationEspacePropose.get(j).getStatut().compareTo(statut)==0) %>">
					<div class="reservation">
						<div class="col" id="col_1">
							<img id="imgAnnonce" src="img/<%=listePhotoAnnoncePropose.get(j).getPhoto()%>" alt="<%=listePhotoAnnoncePropose.get(j).getPhoto()%>">
						</div>
						<div class="col" id="col_2">
							<h4>${annoncePropose.typeEspace} de ${annoncePropose.surface}m2</h4>
							<p>${annoncePropose.ville}</br>${annoncePropose.codePostal}</br>${annoncePropose.adresse}</br>${annoncePropose.surface}m2</br>${annoncePropose.prix}€/${annoncePropose.periodeLocation}</br></p>
						</div>
						<div class="col" id="col_3">
							<p>Statut : En attente de validation</br>Type de contrat :<c:if test="${not annoncePropose.dep_Loc}"> Dépot</c:if><c:if test="${annoncePropose.dep_Loc}">Location</c:if></br>Du <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservationEspacePropose.get(j).getDateDebut() %>" /> au <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservationEspacePropose.get(j).getDateFin() %>" /></br>Locataire : <%=listeLocataire.get(j).getPrenom() %> <%=listeLocataire.get(j).getNom() %></p>
						</div>
						<span style="cursor: pointer;" class="bouton" id="accepter" onclick="confirmationValidation(<%=listeReservationEspacePropose.get(j).getId_reserv()%>);">Accepter</span>
						<span style="cursor: pointer;" class="bouton" id="refuser" onclick="confirmationAnnulation(<%=listeReservationEspacePropose.get(j).getId_reserv()%>);">Refuser</span>
						<span style="cursor: pointer;" class="bouton" id="details_3" onclick='window.location.href="detailsReservation?idReservation=<%=listeReservationEspacePropose.get(j).getId_reserv()%>";'>Détails</span>
					</div>
				</c:if>
				<%j++; %>
			</c:forEach>
			<%j=0; %>
			<%statut="En attente de paiement"; %>
			<c:forEach var="annoncePropose" items="${listeAnnoncePropose}">
				<c:if test="<%=(listeReservationEspacePropose.get(j).getStatut().compareTo(statut)==0) %>">
					<div class="reservation">
						<div class="col" id="col_1">
							<img id="imgAnnonce" src="img/<%=listePhotoAnnoncePropose.get(j).getPhoto()%>" alt="<%=listePhotoAnnoncePropose.get(j).getPhoto()%>">
						</div>
						<div class="col" id="col_2">
							<h4>${annoncePropose.typeEspace} de ${annoncePropose.surface}m2</h4>
							<p>${annoncePropose.ville}</br>${annoncePropose.codePostal}</br>${annoncePropose.adresse}</br>${annoncePropose.surface}m2</br>${annoncePropose.prix}€/${annoncePropose.periodeLocation}</br></p>
						</div>
						<div class="col" id="col_3">
							<p>Statut : En attente de paiement</br>Type de contrat :<c:if test="${not annoncePropose.dep_Loc}"> Dépot</c:if><c:if test="${annoncePropose.dep_Loc}">Location</c:if></br>Du <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservationEspacePropose.get(j).getDateDebut() %>" /> au <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservationEspacePropose.get(j).getDateFin() %>" /></br>Locataire : <%=listeLocataire.get(j).getPrenom() %> <%=listeLocataire.get(j).getNom() %></p>
						</div>
						<span style="cursor: pointer;" class="bouton" id="details_1" onclick='window.location.href="detailsReservation?idReservation=<%=listeReservationEspacePropose.get(j).getId_reserv()%>";'>Détails</span>
					</div>
				</c:if>
				<%j++; %>
			</c:forEach>
			<%j=0; %>
			<%statut="Validee"; %>
			<c:forEach var="annoncePropose" items="${listeAnnoncePropose}">
				<c:if test="<%=(listeReservationEspacePropose.get(j).getStatut().compareTo(statut)==0) %>">
					<div class="reservation">
						<div class="col" id="col_1">
							<img id="imgAnnonce" src="img/<%=listePhotoAnnoncePropose.get(j).getPhoto()%>" alt="<%=listePhotoAnnoncePropose.get(j).getPhoto()%>">
						</div>
						<div class="col" id="col_2">
							<h4>${annoncePropose.typeEspace} de ${annoncePropose.surface}m2</h4>
							<p>${annoncePropose.ville}</br>${annoncePropose.codePostal}</br>${annoncePropose.adresse}</br>${annoncePropose.surface}m2</br>${annoncePropose.prix}€/${annoncePropose.periodeLocation}</br></p>
						</div>
						<div class="col" id="col_3">
							<p>Statut : Validée</br>Type de contrat :<c:if test="${not annoncePropose.dep_Loc}"> Dépot</c:if><c:if test="${annoncePropose.dep_Loc}">Location</c:if></br>Du <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservationEspacePropose.get(j).getDateDebut() %>" /> au <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="<%=listeReservationEspacePropose.get(j).getDateFin() %>" /></br>Locataire : <%=listeLocataire.get(j).getPrenom() %> <%=listeLocataire.get(j).getNom() %></p>
						</div>
						<span style="cursor: pointer;" class="bouton" id="details_1" onclick='window.location.href="attribuerNote?idUserNote=<%=listeLocataire.get(j).getId_user()%>";'>Noter</span>
						<span style="cursor: pointer;" class="bouton" id="details_1" onclick='window.location.href="detailsReservation?idReservation=<%=listeReservationEspacePropose.get(j).getId_reserv()%>";'>Détails</span>
					</div>
				</c:if>
				<%j++; %>
			</c:forEach>
		</c:if>
		</div>
	</body>
</html>
