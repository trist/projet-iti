<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.List"%>
<%@page import="hei.projet.model.AnnonceP"%>
<%@page import="hei.projet.model.Photo"%>
<%@page import="hei.projet.model.Reservation"%>
<%@page import="hei.projet.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
	<title>Détails réservation</title>
	<link rel="stylesheet" type="text/css" href="css/detailsAnnonce.css">
	<link rel="stylesheet" type="text/css" href="css/detailsReservation.css"/>
	<link href="css/navigation.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.4.css" media="screen" /> 
	<script type="text/javascript" src="js/fancybox/jquery-1.4.3.js"></script> 
	<script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
	<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="js/detailsAnnonce.js"></script>
	</head>
	<body style="overflow: auto;" id="body">
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="mesLocations"/>
		</jsp:include>
		<div style="display:none" class="fond" id="fond" onclick="cacherProfil();cacherProfilLocataire();"></div>
		<div style="display:none" class="profil" id="profil">
			<div class="nom">${proprietaire.prenom} ${proprietaire.nom}</div>
			<img class="photo" src="img/${proprietaire.photo}" alt="${proprietaire.photo}" height="150px" width="125px" style="border:1px solid black;"></img>
			<h3>Notes :</h3>
			<%List<User> users = (List) request.getAttribute("noteurs"); %>
			<%int j=0;%>
			<c:forEach var="note" items="${notes}">
				<div class="note">
					<div class="nomUser">De : <%=users.get(j).getPrenom()%> <%=users.get(j).getNom()%></div>
					<span class="noteUser">Note: ${note.note}/5</span>
					<h5>Commentaire :</h5>
					<div class="commentaireUser">${note.commentaire}</div>
				</div>
				<%j++;%>
			</c:forEach>
		</div>
		<div class="detailsAnnonce">
			<div class="details">
				<div class="col" id="col1-images">
					<div class="img1">
						<%List<Photo> photos = (List) request.getAttribute("photos"); %>
						<%AnnonceP annonceP = (AnnonceP) request.getAttribute("annonceP"); %>
						<%User proprietaire = (User) request.getAttribute("telephone"); %>
						<a rel="diaporama_group" href="img/<%=photos.get(0).getPhoto()%>" ><img alt="diapo-exemple" src="img/<%=photos.get(0).getPhoto()%>" width="308px" height="200px"  /></a>
					</div>
					<div class="img2">
						<a rel="diaporama_group" href="img/<%=photos.get(1).getPhoto()%>" ><img alt="diapo-exemple" src="img/<%=photos.get(1).getPhoto()%>" width="100px" height="50px" /></a> 
						<a rel="diaporama_group" href="img/<%=photos.get(2).getPhoto()%>" ><img alt="diapo-exemple" src="img/<%=photos.get(2).getPhoto()%>"width="100px" height="50px" /></a> 
						<a rel="diaporama_group" href="img/<%=photos.get(3).getPhoto()%>" ><img alt="diapo-exemple" src="img/<%=photos.get(3).getPhoto()%>"width="100px" height="50px" /></a>  
					</div>
				</div>
				<div class="col" id="col2">
					<h4>${annonceP.typeEspace} de ${annonceP.surface} m2  </h4>
					<p>${annonceP.ville}</br>${annonceP.codePostal}</br>${annonceP.adresse}</br></br>${annonceP.surface}m2</br>${annonceP.prix}€/${annonceP.periodeLocation}</br></br></br></br></br></br></p>
				</div>
				<div class="col" id="col3">
					<p>Statut : ${reservation.statut} </br></br>Type de contrat : <c:if test="${not annonceP.dep_Loc}">Depot</c:if><c:if test="${annonceP.dep_Loc}">Location</c:if> </br></br>Du <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="${reservation.dateDebut}" /> au <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="${reservation.dateFin}" /></br></br>Locataire: <span class="proprietaire" onclick="afficherProfilLocataire();">${locataire.prenom} ${locataire.nom}</span></br>Propriétaire : <span class="proprietaire" onclick="afficherProfil();">${proprietaire.prenom} ${proprietaire.nom}</span></br></br></br></br></p>
					<span style="cursor: pointer;" class="boutton" onclick='window.location.href="mesLocations"' >Retour page réservations</span>
				</div>
				<div class="description">
					<h3>Description propriétaire :</h3>
					<p>${annonceP.description}</p>
				</div>
				<div class="description">
					<h3>Description locataire :</h3>
					<p>${reservation.description}</p>
				</div>
				<%Reservation reservation = (Reservation) request.getAttribute("reservation"); %>
				<%String statut = "Validee"; %>
				<span style="visibility:<c:if test="<%=(reservation.getStatut().compareTo(statut)!=0) %>">hidden</c:if>;cursor: pointer;" class="bouttonImprimer" id="bouttonImprimer" onclick='window.location.href="creationContrat?idReservation=${reservation.id_reserv}"'>Imprimer le contrat</span>
				<div class="contacter">
					<h3>Contacter :</h3>
					<span style="cursor: pointer;" class="bouttonContacter" onclick='window.location.href="posterMessage?idUser=${telephone.id_user}";'>Par message</span><span style="cursor: pointer;" class="bouttonContacter" onclick="afficherTel('${telephone.numTel}', <% if (proprietaire.isVisibiliteTel()){ %>'true'<%}%><%else{%>'false'<%}%>);">Par téléphone</span> 
				</div>
			</div>	
		</div>
		<div style="display:none" class="profil" id="profilLocataire">
			<div class="nom">${locataire.prenom} ${locataire.nom}</div>
			<img class="photo" src="img/${locataire.photo}" alt="${locataire.photo}" height="150px" width="125px" style="border:1px solid black;"></img>
			<h3>Notes :</h3>
			<%List<User> usersL = (List) request.getAttribute("noteursL"); %>
			<%j=0;%>
			<c:forEach var="noteL" items="${notesL}">
				<div class="note">
					<div class="nomUser">De : <%=usersL.get(j).getPrenom()%> <%=usersL.get(j).getNom()%></div>
					<span class="noteUser">Note: ${noteL.note}/5</span>
					<h5>Commentaire :</h5>
					<div class="commentaireUser">${noteL.commentaire}</div>
				</div>
				<%j++;%>
			</c:forEach>
		</div>
	</body>
</html>