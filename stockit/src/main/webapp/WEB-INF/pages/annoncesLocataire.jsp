<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="hei.projet.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Annonce Locataire</title>
<link rel="stylesheet" type="text/css" href="css/listeAnnoncesLocataire.css">
<link href="css/navigation.css" rel="stylesheet">
</head>
<body>
	<jsp:include page="navigation.jsp">
		<jsp:param name="pageSelectionnee" value="annoncesLocataire"/>
	</jsp:include>
	<div class="listeAnnonces">
		<form method="post" action="annoncesLocataire">
			<div class="champ1">
				<label for="codepostal" class="texte">Code Postal</label>
				<input type="text" class="form-control" name="codepostal" id="codepostal" required/>
			</div>
			<div class="champ1">
				<label for="surface" class="texte">Surface</label>
				<input type="text" class="form-control" name="surface" id="surface"/>
			</div>
			<div class="champ2">
				<label for="typeEspace">Type d'espace : </label>
				<label>Cave</label><input type="checkbox" name="checkbox1" value="cave">
				<label>Grenier</label><input type="checkbox" name="checkbox2" value="grenier">
				<label>Garage</label><input type="checkbox" name="checkbox3" value="garage">
				<label>Pièce à vivre</label><input type="checkbox" name="checkbox4" value="piece a vivre">
			</div>
			<div class="champ2">
				<input class="btn btn-primary" type="submit" value="Rechercher">
			</div>
		</form>
		<div>
			<c:forEach var="annonce" items="${annonces}">
			<div class="annonce">
				<div class="col" id="col_1">
					<h4>Espace de ${annonce.surface}m2</h4>
					<p>Publié le : <fmt:formatDate type="date" dateStyle="short" value="${annonce.datePubl}" /></br></br>${annonce.ville}</br>${annonce.codePostal}</br>${annonce.surface}m2</br>${annonce.prix}€ ${annonce.periodeLoc}</br></p>
				</div>
				<div class="col" id="col_2">
					<p>Actuellement en recherche</p>
				</div>
				<span style="cursor: pointer;" class="details" onclick='window.location.href="detailsAnnonceLocataire?idAnnL=${annonce.id_annl}";'>Détails</span>
			</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>