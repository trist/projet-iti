<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="hei.projet.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Poster un message</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/navigation.css" rel="stylesheet">
		<link href="css/posterMessage.css" rel="stylesheet">
	</head>
	<body>
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="mesLocations"/>
		</jsp:include>
		<div class="posterMessage">
			<h2>Poster un message :</h2>
			<form class="formMessage" method="post" action="posterMessage">
				<label class="destinataire">A : ${destinataire.prenom} ${destinataire.nom} </label></br></br>
				<label class="emetteur">De : ${emetteur.prenom} ${emetteur.nom}</label></br></br>
				<label class="formulaireMessage">Sujet :</label>
				<input type="text" name="sujet" id="sujet" maxlength="60" size="60" required/></br></br>
				<label for="message" class="formulaireMessage">Message : </label>
				<textarea rows="7" cols="50" type="textarea" class="form-control" name="message" id="message"></textarea></br></br>
				<input class="valider" id="valider" type="submit" value="Envoyer">
		</form>
		</div>
	</body>
</html>
