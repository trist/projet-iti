<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="hei.projet.model.User"%>
<%@page import="hei.projet.model.Message"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Mes messages</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link href="css/navigation.css" rel="stylesheet"/>
		<link href="css/mesMessages.css" rel="stylesheet"/>
	</head>
	<body>
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="mesLocations"/>
		</jsp:include>
		<div class="mesMessages">
			<h2>Vos messages :</h2>
			<%List<User> emetteurs = (List) request.getAttribute("emetteurs"); %>
			<%int j=0; %>
			<c:forEach var="message" items="${messages}">
				<div class="message">
					<div class="emetteur">De :<%=emetteurs.get(j).getPrenom()%> <%=emetteurs.get(j).getNom()%></div>
					<div class="sujet">Sujet : ${message.sujet}</div>
					<div class="contenu">${message.contenu}</div>
					<div class="repondre" onclick='window.location.href="posterMessage?idUser=<%=emetteurs.get(j).getId_user()%>";'>Répondre</div>
				</div>
				</div>
			<%j++; %>
			</c:forEach>
		</div>
	</body>
</html>
