<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.List"%>
<%@page import="hei.projet.model.User"%>
<%@page import="hei.projet.model.AnnonceL"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Liste des annonces</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/navigation.css" rel="stylesheet">
		<link href="css/listeUsers.css" rel="stylesheet">
	</head>
	<body>
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="listeAnnoncesLocataireAdmin"/>
		</jsp:include>
		<div class="listeAnnonces">
			<div class="tableau">
				<h3>Liste annonces locataire :</h3>
				<table>
					<thead>
						<th class="col1">Titre Annonce :</th>
						<th class="col2">Date de Publication :</th>
						<th class="col3">Annonceur :</th>
						<th class="col4">Détails</th>
						<th class="col5">Supprimer</th>
					</thead>
					<tbody>
						<%List<User> annonceur = (List) request.getAttribute("listeAnnonceur"); %>
						<%int j=0; %>
						<c:forEach var="annonce" items="${listeAnnonceL}">
							<tr>
								<td>Espace de ${annonce.surface}m2</td>
								<td><fmt:formatDate type="date" dateStyle="short" value="${annonce.datePubl}" /></td>
								<td><%=annonceur.get(j).getPrenom()%> <%=annonceur.get(j).getNom() %></td>
								<td><a href="detailsAnnonceLocataire?idAnnL=${annonce.id_annl}"><img id="detail" src="img/detailIcon.jpg" alt="detailIcon.jpg" title="Détails annonce"></a></td>
								<td><a href="supprimerAnnonceLocataire?idAnnonceL=${annonce.id_annl}"><img id="detail" src="img/supprimer.png" alt="supprimer.png" title="Supprimer annonce"></a></td>
							</tr>
							<%j++; %>
						</c:forEach>
					</tbody>
				</table>
			</div>	
		</div>
	</body>
</html>