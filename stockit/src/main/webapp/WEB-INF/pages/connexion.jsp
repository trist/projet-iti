<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="hei.projet.model.User"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Connexion</title>
<link rel="stylesheet" type="text/css" href="css/connexion.css">
<link href="css/navigation.css" rel="stylesheet">
</head>
<body>
	<jsp:include page="navigation.jsp">
		<jsp:param name="pageSelectionnee" value="connexion"/>
	</jsp:include>
	<div class="page">
	<div class="formulaire" id="connexion">
		<h2>Se connecter</h2>
		<span class="erreur"><c:out value="${loginError}"/></span>
		<form method="post" action="connexion">
			<div class="champ"> 
				<label for="email" class="texte">E-mail</label>
				<div class="input">
					<input type="text" class="form-control" name="email" size="45" id="email" required/>
				</div>
			</div>
			<div class="champ">
				<label for="password" class="texte">Mot de passe</label>
				<div class="input">
					<input type="password" class="form-control" name="password" id="password" required/>
				</div>
				
			</div>
			<div>
				<div class="champ">
					<input class="btn btn-primary" name="connexion" type="submit" value="Se connecter">
				</div>
			</div>
		</form>
	</div>
	<div class="formulaire" id="inscription">
		<h2>S'inscrire</h2>
		<span class="erreur"><c:out value="${loginErrorInscription}"/></span>
		<form method="post" action="connexion">
			<div class="champ">
				<label for="email" class="texte">E-mail</label>
				<div class="input">
					<input type="text" class="form-control" size="45" name="email" id="email" required/>
				</div>
			</div>
			<div class="champ">
				<label for="password" class="texte">Mot de passe</label>
				<div class="input">
					<input type="password" class="form-control" name="password" id="password" required/>
				</div>
			</div>
			<div class="champ">
				<label for="confpassword" class="texte">Confirmer le mot de passe</label>
				<div class="input">
					<input type="password" class="form-control" name="confpassword" id="confpassword" required/>
				</div>
			</div>
			<div class="champ">
				<label for="nom" class="texte">Nom</label>
				<div class="input">
					<input type="text" class="form-control" size="30" name="nom" id="nom" required/>
				</div>
			</div>
			<div class="champ">
				<label for="prenom" class="texte">Prénom</label>
				<div class="input">
					<input type="text" class="form-control" size="30" name="prenom" id="prenom" required/>
				</div>
			</div>
			<div>
				<div class="champ">
					<input class="btn btn-primary" name="inscription" type="submit" value="S'inscrire">
				</div>
			</div>
		</form>
	</div>
	</div>
</body>
</html>