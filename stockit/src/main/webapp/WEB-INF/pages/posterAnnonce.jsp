<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Poster une annonce</title>
<link href="css/navigation.css" rel="stylesheet">
<link href="css/posterAnnonce.css" rel="stylesheet">
<script src="js/posterAnnonce.js" type="text/javascript"></script>
</head>
<body>
	<jsp:include page="navigation.jsp"></jsp:include>
	<div class="posterAnnonce">
		<form action="posterAnnonce" method="post" enctype="multipart/form-data">
			<div class="typeAnnonce">
				<label for="typeAnnonce" class="texte">Vous êtes :</label>
				<select name="typeAnnonce" id="typeAnnonce" onChange="afficherFormulaire();">
					<option name="typeAnnonce" id="typeAnnonce" value="proprietaire">Propriétaire</option>
					<option name="typeAnnonce" id="typeAnnonce" value="locataire">Locataire</option>
				</select>
			</div>
			<div style="display:" class="formulaire" id="formulaireProprietaire">
				<div>
					<label for="codepostal" class="texte">Code Postal : </label>
					<input type="text" class="form-control" name="codePostal" id="codepostal"/>
				</div>
				<div>
					<label for="ville" class="texte">Ville : </label>
					<input type="text" class="form-control" size="30" name="ville" id="ville"/>
				</div>
				<div>
					<label for="adresse" class="texte">Adresse : </label>
					<input type="text" class="form-control" size="45" name="adresse" id="adresse"/>
				</div>
				<div>
					<label for="surface" class="texte">Surface : </label>
					<input type="text" class="form-control" name="surface" id="surface"/>m2<span class="erreur"><c:out value="${surface}" /></span>
				</div>
				<div>
					<label for="type" class="texte">Type de contrat : </label>
					<select class="typeContrat" name="typeContrat">
						<option name="typeContrat" id="type" value="False">Dépot</option>
						<option name="typeContrat" id="type" value="True">Location</option>
					</select>
				</div>
				<div>
					<label for="type" class="texte">Période de location : </label>
					<select class="periodeLocation" name="periodeLocation" id="periodeLocation" onChange="afficherPrix();">
						<option id="type" value="hebdomadaire">Hebdomadaire</option>
						<option id="type" value="mensuel">Mensuelle</option>
					</select>
				</div>
				<div style="display:" id="hebdomadaire">
					<label for="prixHebdomadaire" class="texte">Prix par semaine : </label>
					<input type="text" class="form-control" name="prixHebdomadaire" id="prix"/>€/semaine<span class="erreur"><c:out value="${prixHebdomadaire}"/></span>
				</div>
				<div style="display:none" id="mensuel">
					<label for="prixMensuel" class="texte">Prix par mois : </label>
					<input type="text" class="form-control" name="prixMensuel" id="prix"/>€/mois<span class="erreur"><c:out value="${prixMensuel}"/></span>
				</div>
				<div>
					<label for="typeEspace" class="texte">Type d'espace : </label>
					<select class="form-control" name="typeEspace" id="type">
						<option id="type" value="Cave">Cave</option>
						<option id="type" value="Grenier">Grenier</option>
						<option id="type" value="Garage">Garage</option>
						<option id="type" value="Pièce a vivre">Pièce à vivre</option>
					</select>
				</div>
				<div>
					<label for="description" class="texte">Description : </label>
					<textarea rows="5" cols="35" class="form-control" name="description" id="description"></textarea>
				</div>
				<p>Ajoutez de une à quatres photos (les formats autorisés sont .gif, .png et .jpg) :</p>
				<div>
					<label for="fichier">Photo n°1 : </label>
                	<input type="file" id="fichier" name="fichier1" /><span class="erreur"><c:out value="${fichier1}" /></span>
				</div>
				<div>
					<label for="fichier">Photo n°2 : </label>
                	<input type="file" id="fichier" name="fichier2" /><span class="erreur"><c:out value="${fichier2}" /></span>
				</div>
				<div>
					<label for="fichier">Photo n°3 : </label>
                	<input type="file" id="fichier" name="fichier3" /><span class="erreur"><c:out value="${fichier3}" /></span>
				</div>
				<div>
					<label for="fichier">Photo n°4 : </label>
                	<input type="file" id="fichier" name="fichier4" /><span class="erreur"><c:out value="${fichier4}" /></span>
				</div>
				<div class="champ2">
					<input class="valider" type="submit" value="Valider">
				</div>
			</div>
			<div style="display:none" class="formulaire" id="formulaireLocataire">
				<div>
					<label for="codePostalL" class="texte">Code Postal : </label>
					<input type="text" class="form-control" name="codePostalL" id="codepostalL"/>
				</div>
				<div>
					<label for="villeL" class="texte">Ville : </label>
					<input type="text" class="form-control" size="30" name="villeL" id="villeL"/>
				</div>
				<div>
					<label for="surfaceL" class="texte">Surface : </label>
					<input type="text" class="form-control" name="surfaceL" id="surfaceL"/>m2
				</div>
				<div>
					<label for="periodeLocationL" class="texte">Période de location : </label>
					<select class="periodeLocation" name="periodeLocationL" id="periodeLocationL" onChange="afficherPrixL();">
						<option id="periodeLocationL" value="hebdomadaire">Hebdomadaire</option>
						<option id="periodeLocationL" value="mensuel">Mensuelle</option>
					</select>
				</div>
				<div style="display:" id="hebdomadaireL">
					<label for="prixHebdomadaire" class="texte">Prix par semaine : </label>
					<input type="text" class="form-control" name="prixHebdomadaireL" id="prix"/>€/semaine<c:out value="${prixHebdomadaire}" />
				</div>
				<div style="display:none" id="mensuelL">
					<label for="prixMensuel" class="texte">Prix par mois : </label>
					<input type="text" class="form-control" name="prixMensuelL" id="prix"/>€/mois<c:out value="${prixMensuel}" />
				</div>
				<div>
					<label  for="date" class="texte">Date de début : </label>
					<input type="date" class="form-control" name="datedebut" id="datedebut" placeholder="aaaa-mm-jj"/>
				</div>
				<div>
					<label  for="date" class="texte">Date de fin : </label>
					<input type="date" class="form-control" name="datefin" id="datefin" placeholder="aaaa-mm-jj"/>
				</div>
				<div>
					<label for="typeEspaceL">Type(s) d'espace : </label>
					<input type="checkbox" name="checkbox1" value="cave">Cave
					<input type="checkbox" name="checkbox2" value="grenier">Grenier
					<input type="checkbox" name="checkbox3" value="garage">Garage
					<input type="checkbox" name="checkbox4" value="pieceVivre">Pièce à vivre
				</div>
				<div>
					<label for="descriptionL" class="texte">Description : </label>
					<textarea rows="5" cols="35" type="textarea" class="form-control" name="descriptionL" id="description"></textarea>
				</div>
				<div class="champ2">
					<input class="valider" type="submit" value="Valider">
				</div>
			</div>
		</form>        
	</div>
</body>
</html>