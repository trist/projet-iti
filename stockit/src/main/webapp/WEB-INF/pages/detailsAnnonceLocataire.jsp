<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.List"%>
<%@page import="hei.projet.model.AnnonceL"%>
<%@page import="hei.projet.model.AnnonceP"%>
<%@page import="hei.projet.model.TypeEspace"%>
<%@page import="hei.projet.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Détails annonce</title>
		<link rel="stylesheet" type="text/css" href="css/detailsAnnonceLocataire.css">
		<link href="css/navigation.css" rel="stylesheet">
		<script src="js/detailsAnnonceLocataire.js" type="text/javascript"></script>
		<script src="js/detailsAnnonce.js" type="text/javascript"></script>
	</head>
	<body style="overflow: auto;" id="body">
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="annoncesLocataire"/>
		</jsp:include>
		<div class="detailsAnnonce">
			<div class="details">
				<%User annonceur = (User) request.getAttribute("annonceur"); %>
				<div class="col" id="col_1">
					<h3>Espace de ${annonceL.surface}m2</h3>
					<p>Publié le : <fmt:formatDate type="date" dateStyle="short" value="${annonceL.datePubl}" /></br>Annonceur : ${annonceur.prenom} ${annonceur.nom}</br>${annonceL.ville}</br>${annonceL.codePostal}</br>${annonceL.surface}m2</br>${annonceL.prix}€/${annonceL.periodeLoc}</br></br>Du <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="${annonceL.dateDebut}" /> au <fmt:formatDate type="date" dateStyle="short" timeStyle="short" value="${annonceL.dateFin}" /></p>
				</div>
				<div class="col" id="col_2">
					<p>Type(s) d'espace recherché : <c:forEach var="typeEspace" items="${typeEspace}"> ${typeEspace.typeEspace}</c:forEach></br></br></br></br></br></br></br></p>
				</div>
				<div class="col" id="col_3">
					<div class="contacter">
						<h4>Contacter l'annonceur</h4>
						<span style="cursor: pointer;" class="boutton" onclick='window.location.href="posterMessage?idUser=${annonceur.id_user}";'>Par message</span>
						<span style="cursor: pointer;" class="boutton" onclick="afficherTel('${annonceur.numTel}', <% if (annonceur.isVisibiliteTel()){ %>'true'<%}%><%else{%>'false'<%}%>);">Par téléphone</span>	
					</div>
					<span style="cursor: pointer;" class="bouttonProposer" onclick="afficherProposition();">Proposer un espace</span>
				</div>
				<div class="description">
					<h4>Description :</h4>
					<p>${annonceL.description}</p>
				</div>
			</div>
		</div>
		<div style="display:none" class="fond" id="fond" onclick="cacherProposition();"></div>
		<div style="display:none" class="proposition" id="proposition">
			<%List<AnnonceP> listeAnnonceP = (List) request.getAttribute("listeAnnonceP"); %>
			<h4>Proposer l'une de vos annonces :</h4>
			<c:if test="<%=(listeAnnonceP.size()==0) %>">
				<p>Vous n'avez pas d'annonces à proposer. Veuillez en poster une pour pouvoir répondre à cette annonce.</p>
				<span style="cursor: pointer;" class="posterAnnonce" onclick='window.location.href="posterAnnonce"'>Poster une annonce</span>
			</c:if>
			<c:if test="<%=(listeAnnonceP.size()!=0) %>">
				<c:forEach var="annonceP" items="${listeAnnonceP}">
					<div class="annonceProp">
						<h4>${annonceP.typeEspace} de ${annonceP.surface}m2</h4>
						<p>${annonceP.ville}</p>
						<span style="cursor: pointer;" class="proposer" onclick='window.location.href="repondreAnnonceLocataire?idLocataire=${annonceL.id_user}&idAnnL=${annonceL.id_annl}&idAnnP=${annonceP.id_annp}";'>Proposer cette annonce</span>
					</div>
				</c:forEach>
			</c:if>
		</div>
	</body>
</html>