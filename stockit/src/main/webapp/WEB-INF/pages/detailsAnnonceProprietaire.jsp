<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.List"%>
<%@page import="hei.projet.model.AnnonceP"%>
<%@page import="hei.projet.model.Photo"%>
<%@page import="hei.projet.model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Détails annonce</title>
<link rel="stylesheet" type="text/css" href="css/detailsAnnonce.css">
<link href="css/navigation.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.4.css" media="screen" /> 
<script type="text/javascript" src="js/fancybox/jquery-1.4.3.js"></script> 
<script type="text/javascript" src="js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script src="js/detailsAnnonce.js" type="text/javascript"></script>
<script src="js/localisation.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
</head>
<body style="overflow: auto;" id="body">
	<jsp:include page="navigation.jsp">
	    <jsp:param name="pageSelectionnee" value="annoncesProprietaire"/>
	</jsp:include>
	<div class="detailsAnnonce">
		<div class="details">
			<div class="col" id="col1-images">
				<div class="img1">
					<%List<Photo> photos = (List) request.getAttribute("photos"); %>
					<%AnnonceP annonceP = (AnnonceP) request.getAttribute("annonceP"); %>
					<%User proprietaire = (User) request.getAttribute("proprietaire"); %>
					<a rel="diaporama_group" href="img/<%=photos.get(0).getPhoto()%>" ><img alt="diapo-exemple" src="img/<%=photos.get(0).getPhoto()%>" width="308px" height="200px"  /></a>
				</div>
				<div class="img2">
					<a rel="diaporama_group" href="img/<%=photos.get(1).getPhoto()%>" ><img alt="diapo-exemple" src="img/<%=photos.get(1).getPhoto()%>" width="100px" height="50px" /></a> 
					<a rel="diaporama_group" href="img/<%=photos.get(2).getPhoto()%>" ><img alt="diapo-exemple" src="img/<%=photos.get(2).getPhoto()%>"width="100px" height="50px" /></a> 
					<a rel="diaporama_group" href="img/<%=photos.get(3).getPhoto()%>" ><img alt="diapo-exemple" src="img/<%=photos.get(3).getPhoto()%>"width="100px" height="50px" /></a>  
				</div>
			</div>
			<div class="col" id="col2">
				<h4>${annonceP.typeEspace} de ${annonceP.surface} m2  </h4>
				<p>Publié le : <fmt:formatDate type="date" dateStyle="short" value="${annonceP.datePubl}" /></br>Propriétaire : <span class="proprietaire" onclick="afficherProfil();">${proprietaire.prenom} ${proprietaire.nom}</span></br></br>${annonceP.ville}</br>${annonceP.codePostal}</br>${annonceP.adresse}</br></br>${annonceP.surface}</br>${annonceP.prix}€/${annonceP.periodeLocation}</br></br>En ${depotLocation}</p>
			</div>
			<div class="col" id="col3">
				<div id="contacter">
					<p>Contacter l'annonceur :</p>
					<span style="cursor: pointer;" class="boutton" onclick='window.location.href="posterMessage?idUser=${proprietaire.id_user}";'>Par message</span>
					<span style="cursor: pointer;" class="boutton" onclick="afficherTel('${proprietaire.numTel}', <% if (proprietaire.isVisibiliteTel()){ %>'true'<%}%><%else{%>'false'<%}%>);">Par téléphone</span>
				</div>
				<div id="reserver">
					</br></br></br>
					<div style="cursor: pointer;" class="bouton" onclick="afficherReservation();">Réserver</div>
					</br>
				</div>
			</div>
			<div style="cursor: pointer;" class="lien" id="lien1" onclick="afficherDescription();">Description</div>
			<div style="cursor: pointer;" class="lien" id="lien2" onclick="afficherLocalisation();initialize();codeAddress('<%=annonceP.getAdresse().replace("'", " ")%>, ${annonceP.codePostal} <%=annonceP.getVille().replace("'", " ")%>');">Localisation</div>
			<div style="display:" class="information" id="description">
				<p>${annonceP.description}</p>
			</div>
			<div style="display:none" class="information" id="localisation">
				<div id="map-canvas"></div>
			</div>
		</div>	
	</div>
	<div style="display:none" class="fond" id="fond" onclick="cacherReservation();cacherProfil();"></div>
	<div style="display:none" class="reservation" id="reservation">
		<h3>Réservation :</h3>
		<form method="post" action="detailsAnnonceProprietaire">
			<label  for="dateFin" class="texte">Date de début : </label>
			<input type="date" class="form-control" name="dateDebut" id="datedebut" placeholder="aaaa-mm-jj"/></br></br>
			<label  for="dateDebut" class="texte">Date de fin : </label>
			<input type="date" class="form-control" name="dateFin" id="datefin" placeholder="aaaa-mm-jj"/></br></br>
			<label for="description" class="texte">Description : </label>
			<textarea rows="5" cols="35" type="textarea" class="form-control" name="description" id="description"></textarea></br></br>
			<div class="validerAnnuler">
				<input class="valider" id="valider" type="submit" value="Valider">
				<input class="valider" id="annuler" type="button" value="Annuler" onclick="cacherReservation();">
			</div>			
		</form>
	</div>
	<div style="display:none" class="profil" id="profil">
		<div class="nom">${proprietaire.prenom} ${proprietaire.nom}</div>
		<img class="photo" src="img/${proprietaire.photo}" alt="${proprietaire.photo}" height="150px" width="125px" border="1px solid black">
		<h3>Notes :</h3>
		<%List<User> users = (List) request.getAttribute("noteurs"); %>
		<%int j=0;%>
		<c:forEach var="note" items="${notes}">
			<div class="note">
				<div class="nomUser">De : <%=users.get(j).getPrenom()%> <%=users.get(j).getNom()%></div>
				<span class="noteUser">Note: ${note.note}/5</span>
				<h5>Commentaire :</h5>
				<div class="commentaireUser">${note.commentaire}</div>
			</div>
			<%j++;%>
		</c:forEach>
	</div>
</body>