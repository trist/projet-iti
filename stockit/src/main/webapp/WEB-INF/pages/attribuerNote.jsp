<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="hei.projet.model.User"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Mes locations</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/navigation.css" rel="stylesheet">
		<link href="css/attribuerNote.css" rel="stylesheet">
	</head>
	<body>
		<jsp:include page="navigation.jsp">
			<jsp:param name="pageSelectionnee" value="mesLocations"/>
		</jsp:include>
		<div class="noter">
			<h3>Donner une note à ${user.prenom} ${user.nom}</h3>
			<form method="post" action="attribuerNote">
			<label for="note" class="texte">Note :</label>
			<select name="note" id="note">
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
			</select></br></br>
			<label for="commentaire" class="texte">Commentaire : </label>
			<textarea rows="5" cols="35" class="form-control" name="commentaire" id="commentaire"></textarea></br></br>
			<input class="valider" type="submit" value="Valider">
			</form>
		</div>
	</body>
</html>