<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="hei.projet.model.User"%>
<%@page import="hei.projet.model.Photo"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Annonces propriétaire</title>
<link rel="stylesheet" type="text/css" href="css/listeAnnoncesProprietaire.css">
<link href="css/navigation.css" rel="stylesheet">
</head>
<body>
	<jsp:include page="navigation.jsp">
		<jsp:param name="pageSelectionnee" value="annoncesProprietaire"/>
	</jsp:include>
	<div class="listeAnnonces">
		<form method="post" action="annoncesProprietaire">
			<div class="champ1">
				<label for="codepostal" class="texte">Code Postal</label>
				<input size="5" type="text" class="form-control" name="codepostal" id="codepostal" required/>
			</div>
			<div class="champ1">
				<label for="typeContrat" class="texte">Type de contrat</label>
				<select class="form-control" name="type" id="type">
					<option name="type" id="type" value="">Indiferent</option>
					<option name="type" id="type" value="1">Dépot</option>
					<option name="type" id="type" value="0">Location</option>
				</select>
			</div>
			<div class="champ1">
				<label for="surface" class="texte">Surface</label>
				<input type="text" class="form-control" name="surface" id="surface"/>
			</div>
			<div class="champ2">
				<label for="typeEspace">Type d'espace : </label>
				<label>Cave</label><input type="checkbox" name="checkbox1" value="cave">
				<label>Grenier</label><input type="checkbox" name="checkbox2" value="grenier">
				<label>Garage</label><input type="checkbox" name="checkbox3" value="garage">
				<label>Pièce à vivre</label><input type="checkbox" name="checkbox4" value="piece a vivre">
			</div>
			<div class="champ2">
				<input class="btn btn-primary" type="submit" value="Rechercher">
			</div>
		</form>
		<div class="AnnoncesProprietaires">
		<%List<Photo> photos = (List) request.getAttribute("listePhoto"); %>
		<%int j=0; %>
		<c:forEach var="annonce" items="${annonces}">
		
			<div class="annonce">
				<div class="col" id="col_1">
					<img id="imgAnnonce" src="img/<%=photos.get(j).getPhoto()%>" alt="<%=photos.get(j).getPhoto()%>">
					<%j++; %>
				</div>
				<div class="col" id="col_2">
					<h4>${annonce.typeEspace} de ${annonce.surface}m2</h4>
					<p>Publié le : <fmt:formatDate type="date" dateStyle="short" value="${annonce.datePubl}" /></br>${annonce.ville}</br>${annonce.codePostal}</br>${annonce.adresse}</br>${annonce.surface}m2</br>${annonce.prix}€ ${annonce.periodeLocation}</p>
				</div>
				<div class="col" id="col_3">
					<c:if test="${not annonce.dep_Loc}">Depot</c:if><c:if test="${annonce.dep_Loc}">Location</c:if><p></br>Disponible maintenant</p>
				</div>
				<span style="cursor: pointer;" class="details" onclick='window.location.href="detailsAnnonceProprietaire?idAnnP=${annonce.id_annp}";'>Détails</span>
			</div>
		</c:forEach>
		</div>
	</div>
</body>
</html>