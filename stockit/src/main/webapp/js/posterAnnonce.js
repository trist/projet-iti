function afficherFormulaire(){
   var selectElmt = document.getElementById("typeAnnonce");
   var valeurselectionnee = selectElmt.options[selectElmt.selectedIndex].value;
   var elmt0 = document.getElementById("formulaireProprietaire");
   var elmt1 = document.getElementById("formulaireLocataire");
   if(valeurselectionnee == "proprietaire"){
	   elmt1.style.display = "none";
	   elmt0.style.display = "";
   }
   else{
	   if(valeurselectionnee == "locataire"){
		   elmt0.style.display = "none";
		   elmt1.style.display = "";
	   }
   }
}

function afficherPrix(){
	var selectElmt1 = document.getElementById("periodeLocation");
	var valeurselectionnee1 = selectElmt1.options[selectElmt1.selectedIndex].value;
	var elmt2 = document.getElementById("hebdomadaire");
	var elmt3 = document.getElementById("mensuel");
	if(valeurselectionnee1 == "hebdomadaire"){
		elmt3.style.display = "none";
		elmt2.style.display = "";
	}
	else{
		if(valeurselectionnee1 == "mensuel"){
			elmt2.style.display = "none";
			elmt3.style.display = "";
		}
	}
}

function afficherPrixL(){
	var selectElmt1 = document.getElementById("periodeLocationL");
	var valeurselectionnee1 = selectElmt1.options[selectElmt1.selectedIndex].value;
	var elmt2 = document.getElementById("hebdomadaireL");
	var elmt3 = document.getElementById("mensuelL");
	if(valeurselectionnee1 == "hebdomadaire"){
		elmt3.style.display = "none";
		elmt2.style.display = "";
	}
	else{
		if(valeurselectionnee1 == "mensuel"){
			elmt2.style.display = "none";
			elmt3.style.display = "";
		}
	}
}