function afficherDescription(){
   var elmt1 = document.getElementById("description");
   var elmt2 = document.getElementById("localisation");
   elmt1.style.display = "";
   elmt2.style.display="none";
}

function afficherLocalisation(){
	   var elmt1 = document.getElementById("description");
	   var elmt2 = document.getElementById("localisation");
	   elmt1.style.display = "none";
	   elmt2.style.display="";
}

$(document).ready(function() { 
	   $('a[rel=diaporama_group]').fancybox({ 
	      'transitionIn' : 'elastic', 
	      'transitionOut' : 'none', 
	   }); 
	}); 

function afficherReservation(){
	   var elmt1 = document.getElementById("fond");
	   var elmt2 = document.getElementById("reservation");
	   var elmt3 = document.getElementById("body");
	   elmt1.style.display = "";
	   elmt2.style.display = "";
	   elmt3.style.overflow = "auto";
}

function cacherReservation(){
	   var elmt1 = document.getElementById("fond");
	   var elmt2 = document.getElementById("reservation");
	   var elmt3 = document.getElementById("body");
	   elmt1.style.display = "none";
	   elmt2.style.display = "none";
	   elmt3.style.overflow = "auto";
}

function afficherTel(telephone, visibiliteTel){
	var visible="true";
	if(visibiliteTel==visible){
		window.alert("Le numéro de téléphone du propriétaire est : "+telephone);
	}else{
		window.alert("Le propriétaire n'a pas souhaité communiqué son numéro de téléphone.");
	}
}

function afficherProfil(){
	 var elmt1 = document.getElementById("fond");
	 var elmt2 = document.getElementById("profil");
	 var elmt3 = document.getElementById("body");
	 elmt1.style.display = "";
	 elmt2.style.display = "";
	 elmt3.style.overflow = "auto";
}

function cacherProfil(){
	var elmt1 = document.getElementById("fond");
	var elmt2 = document.getElementById("profil");
	var elmt3 = document.getElementById("body");
	elmt1.style.display = "none";
	elmt2.style.display = "none";
	elmt3.style.overflow = "auto";
}

function afficherProfilLocataire(){
	 var elmt1 = document.getElementById("fond");
	 var elmt2 = document.getElementById("profilLocataire");
	 var elmt3 = document.getElementById("body");
	 elmt1.style.display = "";
	 elmt2.style.display = "";
	 elmt3.style.overflow = "auto";
}

function cacherProfilLocataire(){
	var elmt1 = document.getElementById("fond");
	var elmt2 = document.getElementById("profilLocataire");
	var elmt3 = document.getElementById("body");
	elmt1.style.display = "none";
	elmt2.style.display = "none";
	elmt3.style.overflow = "auto";
}
