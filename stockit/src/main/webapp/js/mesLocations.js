function confirmationValidation(idReservation){
	if(confirm("Confirmer vous la validation de la réservation?")==true){
		window.location.href="changerStatutReservation?statut=En attente de paiement&idReservation="+idReservation;
	}
}

function confirmationPaiement(idReservation){
	if(confirm("Confirmer vous le paiement?")==true){
		window.location.href="changerStatutReservation?statut=Validee&idReservation="+idReservation;
	}
}

function confirmationAnnulation(idReservation){
	if(confirm("Confirmer vous l'annulation de cette réservation?")==true){
		window.location.href="changerStatutReservation?idReservation="+idReservation;
	}
}

function confirmationSuppressionAnnonceP(idAnnonceP){
	if(confirm("Confirmer vous la suppression de cette annonce?")==true){
		window.location.href="mesAnnonces?idAnnonceP="+idAnnonceP;
	}
}

function confirmationSuppressionAnnonceL(idAnnonceL){
	if(confirm("Confirmer vous la suppression de cette annonce?")==true){
		window.location.href="mesAnnonces?idAnnonceL="+idAnnonceL;
	}
}