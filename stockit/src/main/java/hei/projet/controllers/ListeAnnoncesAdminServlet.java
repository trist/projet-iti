package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceP;
import hei.projet.model.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListeAnnoncesAdminServlet
 */
@WebServlet("/ListeAnnoncesAdminServlet")
public class ListeAnnoncesAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeAnnoncesAdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<AnnonceP> listeAnnonce = Manager.getInstance().listerAnnonceP();
		request.setAttribute("listeAnnnonceP", listeAnnonce);
		List<User> listeProprietaire = new ArrayList<User>();
		if(listeAnnonce.size()!=0){
			for(int i=0; i < listeAnnonce.size(); i++){
				listeProprietaire.add(Manager.getInstance().getInformationUser(listeAnnonce.get(i).getId_user()));
			}
		}
		request.setAttribute("listeProprietaire", listeProprietaire);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/listeAnnoncesAdmin.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
