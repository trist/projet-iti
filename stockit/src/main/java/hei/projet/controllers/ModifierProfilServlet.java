package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.User;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 * Servlet implementation class ModifierProfilServlet
 */
@WebServlet("/ModifierProfilServlet")
public class ModifierProfilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static Integer idUser;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifierProfilServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		
		idUser = userConnecte.getId_user();
		
		User userInformations = Manager.getInstance().getInformationUser(idUser);
		
		request.setAttribute("userInformations", userInformations);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/modifierProfil.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public static final int TAILLE_TAMPON = 10240; // 10 ko
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String adresse = request.getParameter("adresse");
		String ville = request.getParameter("ville");
		String codePostal = request.getParameter("codePostal");
		Date dateN = null;
		try {
			dateN = dateFormat.parse(request.getParameter("dateNaissance"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String numTel = request.getParameter("numTel");
		Boolean visibiliteTel = Boolean.valueOf(request.getParameter("visibiliteTel"));
		
		String chemin = this.getServletConfig().getInitParameter("chemin");
		
		Part part = request.getPart("fichier");
    	/*
	     * Il faut déterminer s'il s'agit d'un champ classique 
	     * ou d'un champ de type fichier : on délègue cette opération 
	     * à la méthode utilitaire getNomFichier().
	     */
	    String nomFichier = getNomFichier( part );
	    /*
	     * Si la méthode a renvoyé quelque chose, il s'agit donc d'un champ
	     * de type fichier (input type="file").
	     */
	    if ( nomFichier != null && !nomFichier.isEmpty() ) {
	    	nomFichier = nomFichier.substring( nomFichier.lastIndexOf( '/' ) + 1 ).substring( nomFichier.lastIndexOf( '\\' ) + 1 );
	    	int verificationExtension = verifierExtension(nomFichier);
	    	if(verificationExtension==1){
	    		nomFichier = creerNomFichier(nomFichier);
		        /* Écriture du fichier sur le disque */
		        ecrireFichier( part, nomFichier, chemin );	
	    	}else{
	    		request.setAttribute("fichier", "Ce type de fichier n'est pas autorisé." );
	    	}
	    }
	    
	    if(nom!=null && prenom!=null && mail !=null && adresse !=null && ville !=null && codePostal !=null && dateN!=null){
		    User informationsUser = new User(idUser, nom, prenom, mail,dateN, numTel, visibiliteTel, adresse, ville, codePostal, nomFichier);
		    Manager.getInstance().modifierUser(informationsUser);
		    response.sendRedirect("monProfil");
	    }else{
	    	response.sendRedirect("modifierProfil");
	    }
	}
	/* 
	 * Méthode utilitaire qui a pour unique but d'analyser l'en-tête "content-disposition",
	 * et de vérifier si le paramètre "filename"  y est présent. Si oui, alors le champ traité
	 * est de type File et la méthode retourne son nom, sinon il s'agit d'un champ de formulaire 
	 * classique et la méthode retourne null. 
	 */
	private static String getNomFichier( Part part ) {
	    /* Boucle sur chacun des paramètres de l'en-tête "content-disposition". */
	    for ( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) {
	    	/* Recherche de l'éventuelle présence du paramètre "filename". */
	        if ( contentDisposition.trim().startsWith("filename") ) {
	            /* Si "filename" est présent, alors renvoi de sa valeur, c'est-à-dire du nom de fichier. */
	            return contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
	        }
	    }
	    /* Et pour terminer, si rien n'a été trouvé... */
	    return null;
	}
	
	private static int verifierExtension(String nomFichier){
		int longueurChaine = nomFichier.length();
		String extension = nomFichier.substring(longueurChaine-4, longueurChaine);
		if(extension.compareTo(".png")==0 || extension.compareTo(".jpg")==0 || extension.compareTo(".gif")==0){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	private static String creerNomFichier(String nomFichier){
		int longueurChaine = nomFichier.length();
		String fileName = nomFichier.substring(0, longueurChaine-5);
		String extension = nomFichier.substring(longueurChaine-4,longueurChaine);
		nomFichier = fileName+System.currentTimeMillis()+extension;
		return nomFichier;
	}
	
	private void ecrireFichier( Part part, String nomFichier, String chemin ) throws IOException {
	    /* Prépare les flux. */
	    BufferedInputStream entree = null;
	    BufferedOutputStream sortie = null;
	    try {
	        /* Ouvre les flux. */
	        entree = new BufferedInputStream( part.getInputStream(), TAILLE_TAMPON );
	        sortie = new BufferedOutputStream( new FileOutputStream( new File( chemin + nomFichier ) ),
	                TAILLE_TAMPON );
	 
	        /* ... */
	        /*
	         * Lit le fichier reçu et écrit son contenu dans un fichier sur le
	         * disque.
	         */
	        byte[] tampon = new byte[TAILLE_TAMPON];
	        int longueur;
	        while ( ( longueur = entree.read( tampon ) ) > 0 ) {
	            sortie.write( tampon, 0, longueur );
	        }
	    } finally {
	        try {
	            sortie.close();
	        } catch ( IOException ignore ) {
	        }
	        try {
	            entree.close();
	        } catch ( IOException ignore ) {
	        }
	    }
	}

}
