package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceL;
import hei.projet.model.AnnonceP;
import hei.projet.model.Photo;
import hei.projet.model.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MesAnnoncesServlet
 */
@WebServlet("/MesAnnoncesServlet")
public class MesAnnoncesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MesAnnoncesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idAnnonceP = request.getParameter("idAnnonceP");
		String idAnnonceL = request.getParameter("idAnnonceL");
		
		if(idAnnonceP != null){
			Integer idAnnp = Integer.parseInt(idAnnonceP);
			Manager.getInstance().supprimerAnnonceP(idAnnp);
		}
		
		if(idAnnonceL!=null){
			Integer idAnnl = Integer.parseInt(idAnnonceL);
			Manager.getInstance().supprimerAnnonceL(idAnnl);
		}
		
		List<AnnonceP> listeAnnonceP = new ArrayList<AnnonceP>();
		List<AnnonceL> listeAnnonceL = new ArrayList<AnnonceL>();
		List<Photo> listePhoto = new ArrayList<Photo>();
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		listeAnnonceP=Manager.getInstance().listerAnnonceP(userConnecte.getId_user());
		listeAnnonceL=Manager.getInstance().listerAnnonceL(userConnecte.getId_user());
		for (int i=0;listeAnnonceP.size()>i;i++){
			listePhoto.add(Manager.getInstance().getPhotoUnique(listeAnnonceP.get(i).getId_annp()));
		}
		request.setAttribute("annoncesP", listeAnnonceP);
		request.setAttribute("annoncesL", listeAnnonceL);
		request.setAttribute("listePhoto",listePhoto);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/mesAnnonces.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
