package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.NoteUser;
import hei.projet.model.User;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AttribuerNoteServlet
 */
@WebServlet("/AttribuerNoteServlet")
public class AttribuerNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Integer idUserNote;
	private Integer idUser;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AttribuerNoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		idUserNote = Integer.parseInt(request.getParameter("idUserNote"));
		
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		idUser = userConnecte.getId_user();
		
		User userNote = Manager.getInstance().getInformationUser(idUserNote);
		request.setAttribute("user", userNote);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/attribuerNote.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Integer note = Integer.parseInt(request.getParameter("note"));
		String commentaire = request.getParameter("commentaire");
		
		NoteUser noteUser = new NoteUser(null,note,commentaire,idUser,idUserNote);
		Manager.getInstance().ajouterNote(noteUser);
		response.sendRedirect("mesLocations");
	}

}
