package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.Reservation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ChangerStatutReservationServlet
 */
@WebServlet("/ChangerStatutReservationServlet")
public class ChangerStatutReservationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangerStatutReservationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String statut = request.getParameter("statut");
		Integer idReservation = Integer.parseInt(request.getParameter("idReservation"));
		Reservation reservation = Manager.getInstance().getReservation(idReservation);
		
		if(statut!=null){
			Manager.getInstance().changerStatutReservation(idReservation, statut);
		}else{
			Manager.getInstance().annulerReservation(idReservation);
			Manager.getInstance().rendreVisibleAnnonceP(reservation.getId_annp());
		}
		
		response.sendRedirect("mesLocations");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
