package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceL;
import hei.projet.model.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListeAnnoncesLocataireAdminServlet
 */
@WebServlet("/ListeAnnoncesLocataireAdminServlet")
public class ListeAnnoncesLocataireAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeAnnoncesLocataireAdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<AnnonceL> listeAnnonceL = Manager.getInstance().listerAnnonceL();
		request.setAttribute("listeAnnonceL", listeAnnonceL);
		List<User> listeAnnonceur = new ArrayList<User>();
		if(listeAnnonceL.size()!=0){
			for(int i=0; i < listeAnnonceL.size(); i++){
				listeAnnonceur.add(Manager.getInstance().getInformationUser(listeAnnonceL.get(i).getId_user()));
			}
		}
		request.setAttribute("listeAnnonceur", listeAnnonceur);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/listeAnnoncesLocataireAdmin.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
