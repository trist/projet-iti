package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.Message;
import hei.projet.model.User;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class PosterMessageServlet
 */
@WebServlet("/PosterMessageServlet")
public class PosterMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Integer id_Destinataire;
	private Integer id_Emetteur;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PosterMessageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		
		id_Emetteur = userConnecte.getId_user();
		User emetteur = Manager.getInstance().getInformationUser(id_Emetteur);
		request.setAttribute("emetteur",emetteur);
		
		id_Destinataire = Integer.parseInt(request.getParameter("idUser"));
		User destinataire = Manager.getInstance().getInformationUser(id_Destinataire);
		request.setAttribute("destinataire", destinataire);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/posterMessage.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String sujet = request.getParameter("sujet");
		String contenu = request.getParameter("message");
		
		Message message = new Message(null, sujet, contenu, id_Emetteur, id_Destinataire);
		if(sujet != null && contenu !=null){
			Manager.getInstance().envoyerMessage(message);
			response.sendRedirect("accueil");
		}else{
			response.sendRedirect("posterMessage?idUser="+id_Destinataire);
		}
	}

}
