package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceP;
import hei.projet.model.Photo;
import hei.projet.model.User;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 * Servlet implementation class ModifierAnnonceProprietaireServlet
 */
@WebServlet("/ModifierAnnonceProprietaireServlet")
public class ModifierAnnonceProprietaireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Integer idAnnp;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifierAnnonceProprietaireServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		idAnnp = Integer.parseInt(request.getParameter("idAnnP"));
		AnnonceP annonceP = Manager.getInstance().getAnnonceP(idAnnp);
		request.setAttribute("annonceP", annonceP);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/modifierAnnonceProprietaire.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public static final int TAILLE_TAMPON = 10240; // 10 ko
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		int nombreErreur =0;
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String adresse = request.getParameter("adresse");
		Integer surface = null;
	    try {
	    	surface = Integer.parseInt(request.getParameter("surface"));
		} catch (NumberFormatException e) {
			request.setAttribute("surface", "Vous devez saisir une valeur numérique." );
		}
	    Boolean typeContrat = Boolean.valueOf(request.getParameter("typeContrat"));
	    String periodeLocation = request.getParameter("periodeLocation");
	    Integer prix=null;
	    try{
	    	if(periodeLocation.compareTo("hebdomadaire") == 0){
		    	try {
		    		prix = Integer.parseInt(request.getParameter("prixHebdomadaire"));
				} catch (NumberFormatException e) {
					request.setAttribute("prixHebdomadaire", "Vous devez saisir une valeur numérique." );
					nombreErreur++;
				}
		    }
	    	
	    	if(periodeLocation.compareTo("mensuel") == 0){
		    	try {
		    		prix = Integer.parseInt(request.getParameter("prixMensuel"));
				} catch (NumberFormatException e) {
					request.setAttribute("prixMensuel", "Vous devez saisir une valeur numérique." );
					nombreErreur++;
				}
		    }
	    }catch(NullPointerException e){
	    	nombreErreur++;
	    }
	    
	    String typeEspace = request.getParameter("typeEspace");
	    String description = request.getParameter("description");
	    List<String> listeChampFichier = new ArrayList<String>();
	    listeChampFichier.add("fichier1");
	    listeChampFichier.add("fichier2");
	    listeChampFichier.add("fichier3");
	    listeChampFichier.add("fichier4");
	    List<Photo> liste = new ArrayList<Photo>();
	    /*
	     * Lecture du paramètre 'chemin' passé à la servlet via la déclaration
	     * dans le web.xml
	     */
	    String chemin = this.getServletConfig().getInitParameter("chemin");
	    
	    for(int i=0;i<listeChampFichier.size();i++){
	    	//Boucle de lecture des quatres champs fichier
	    	Part part = request.getPart(listeChampFichier.get(i));
	    	/*
		     * Il faut déterminer s'il s'agit d'un champ classique 
		     * ou d'un champ de type fichier : on délègue cette opération 
		     * à la méthode utilitaire getNomFichier().
		     */
		    String nomFichier = getNomFichier( part );
		    /*
		     * Si la méthode a renvoyé quelque chose, il s'agit donc d'un champ
		     * de type fichier (input type="file").
		     */
		    if ( nomFichier != null && !nomFichier.isEmpty() ) {
		    	nomFichier = nomFichier.substring( nomFichier.lastIndexOf( '/' ) + 1 ).substring( nomFichier.lastIndexOf( '\\' ) + 1 );
		    	int verificationExtension = verifierExtension(nomFichier);
		    	if(verificationExtension==1){
		    		nomFichier = creerNomFichier(nomFichier);
		    		Photo nouvelPhoto = new Photo(null,nomFichier);
			    	liste.add(nouvelPhoto);
			        /* Écriture du fichier sur le disque */
			        ecrireFichier( part, nomFichier, chemin );	
		    	}else{
		    		request.setAttribute(listeChampFichier.get(i), "Ce type de fichier n'est pas autorisé." );
		    		nombreErreur++;
		    	}
		    }
	    }
	    
	    if(nombreErreur==0){
	    	AnnonceP nouvelAnnonceP = new AnnonceP(idAnnp, null,typeContrat,periodeLocation,typeEspace,surface,adresse,prix,description,ville,codePostal,null,userConnecte.getId_user());
			Manager.getInstance().modifierAnnonceP(nouvelAnnonceP,liste);
		    
		    response.sendRedirect("mesAnnonces");
		    
		    }
	    else{
	    	response.sendRedirect("modifierAnnonceProprietaire?idAnnP="+idAnnp);
			}
	    
	}
	/* 
	 * Méthode utilitaire qui a pour unique but d'analyser l'en-tête "content-disposition",
	 * et de vérifier si le paramètre "filename"  y est présent. Si oui, alors le champ traité
	 * est de type File et la méthode retourne son nom, sinon il s'agit d'un champ de formulaire 
	 * classique et la méthode retourne null. 
	 */
	private static String getNomFichier( Part part ) {
	    /* Boucle sur chacun des paramètres de l'en-tête "content-disposition". */
	    for ( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) {
	    	/* Recherche de l'éventuelle présence du paramètre "filename". */
	        if ( contentDisposition.trim().startsWith("filename") ) {
	            /* Si "filename" est présent, alors renvoi de sa valeur, c'est-à-dire du nom de fichier. */
	            return contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
	        }
	    }
	    /* Et pour terminer, si rien n'a été trouvé... */
	    return null;
	}
	
	private static int verifierExtension(String nomFichier){
		int longueurChaine = nomFichier.length();
		String extension = nomFichier.substring(longueurChaine-4, longueurChaine);
		if(extension.compareTo(".png")==0 || extension.compareTo(".jpg")==0 || extension.compareTo(".gif")==0){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	private static String creerNomFichier(String nomFichier){
		int longueurChaine = nomFichier.length();
		String fileName = nomFichier.substring(0, longueurChaine-5);
		String extension = nomFichier.substring(longueurChaine-4,longueurChaine);
		nomFichier = fileName+System.currentTimeMillis()+extension;
		return nomFichier;
	}
	
	private void ecrireFichier( Part part, String nomFichier, String chemin ) throws IOException {
	    /* Prépare les flux. */
	    BufferedInputStream entree = null;
	    BufferedOutputStream sortie = null;
	    try {
	        /* Ouvre les flux. */
	        entree = new BufferedInputStream( part.getInputStream(), TAILLE_TAMPON );
	        sortie = new BufferedOutputStream( new FileOutputStream( new File( chemin + nomFichier ) ),
	                TAILLE_TAMPON );
	 
	        /* ... */
	        /*
	         * Lit le fichier reçu et écrit son contenu dans un fichier sur le
	         * disque.
	         */
	        byte[] tampon = new byte[TAILLE_TAMPON];
	        int longueur;
	        while ( ( longueur = entree.read( tampon ) ) > 0 ) {
	            sortie.write( tampon, 0, longueur );
	        }
	    } finally {
	        try {
	            sortie.close();
	        } catch ( IOException ignore ) {
	        }
	        try {
	            entree.close();
	        } catch ( IOException ignore ) {
	        }
	    }
	}

}
