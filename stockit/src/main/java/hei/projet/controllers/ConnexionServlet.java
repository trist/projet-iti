package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.User;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Servlet implementation class LoginServlet
 */
public class ConnexionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConnexionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void init() throws ServletException {
		super.init();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/connexion.jsp");
		view.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String connexion=request.getParameter("connexion");
		String inscription=request.getParameter("inscription");
		if (connexion!=null){
		String emailRecup = request.getParameter("email");
		String passwordRecup = request.getParameter("password");
		User user = new User(emailRecup, passwordRecup);
		if (Manager.getInstance().UtilisateurExiste(user)) {
			String passwordStored = Manager.getInstance().getUser(emailRecup).getPassword();
			if(BCrypt.checkpw(passwordRecup, passwordStored)){
				User userConnecte = Manager.getInstance().getUser(emailRecup);
				HttpSession session = request.getSession(true);
				session.setAttribute("utilisateurConnecte", userConnecte);
				response.sendRedirect("monProfil");
			}
			else{
				request.setAttribute("loginError", "Votre mot de passe n'est pas bon. Veuillez rentrer un mot de passe valide.");
				RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/connexion.jsp");
				view.forward(request, response);
			}
		}
		else
		{
			request.setAttribute("loginError", "Votre E-mail n'est pas bon. Veuillez rentrer un E-mail valide.");
			RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/connexion.jsp");
			view.forward(request, response);
		}
		}
		else if (inscription!= null){
			String email = request.getParameter("email");
	        String motDePasse = request.getParameter("password");
	        String confirmation = request.getParameter( "confpassword");
	        if(Manager.getInstance().EmailExiste(email) && EmailValidator.getInstance().isValid(email)){
	        	if(confirmation.compareTo(motDePasse)==0){//Vérification de l'églité des passwords.
			        String hashMotDePasse = BCrypt.hashpw(motDePasse, BCrypt.gensalt(10));
			        String nom = request.getParameter("nom");
			        String prenom = request.getParameter("prenom");
			        User newuser = new User(null, nom, prenom, email, hashMotDePasse);
			        int idUser = Manager.getInstance().ajouterUtilisateur(newuser);
			        newuser.setId_user(idUser);
			        HttpSession session = request.getSession(true);
					session.setAttribute("utilisateurConnecte", newuser);
			        response.sendRedirect("monProfil");
		        }
	        	else{
	        		request.setAttribute("loginErrorInscription", "Les mots de passe ne sont pas identiques.");
	        		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/connexion.jsp");
					view.forward(request, response);
	        	}
	        }
	        else{
	        	request.setAttribute("loginErrorInscription", "Votre E-mail n'est pas valide ou est déja utilisé.");
	        	RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/connexion.jsp");
				view.forward(request, response);
	        }
	        
			
		}

	}



}