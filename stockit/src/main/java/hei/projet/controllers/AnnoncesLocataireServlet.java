package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceL;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AnnoncesLocataireServlet
 */
@WebServlet("/AnnoncesLocataireServlet")
public class AnnoncesLocataireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnnoncesLocataireServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String codePostal = request.getParameter("codP");


		
		String surface = request.getParameter("surf");
		String check1 = request.getParameter("check1");
		String check2 = request.getParameter("check2");
		String check3 = request.getParameter("check3");
		String check4 = request.getParameter("check4");
		List<AnnonceL> annoncesL = new ArrayList<AnnonceL>();
		annoncesL = Manager.getInstance().listerAnnonceL();
		if (codePostal==null && surface==null && check1==null && check2==null && check3==null && check4==null) {
			annoncesL = Manager.getInstance().listerAnnonceL();
		}
		else if (surface.compareTo("")==0 && (check1.compareTo("cave")!=0 && check2.compareTo("grenier")!=0 && check3.compareTo("garage")!=0 && check4.compareTo("piece a vivre")!=0)){
			annoncesL = Manager.getInstance().listerAnnonceL(codePostal);
		}
		else if (check1.compareTo("cave")!=0 && check2.compareTo("grenier")!=0 && check3.compareTo("garage")!=0 && check4.compareTo("piece a vivre")!=0) {
			Integer surfaceint = Integer.parseInt(surface);
			annoncesL = Manager.getInstance().listerAnnonceL(codePostal, surfaceint);
		}
		else if (surface.compareTo("")==0){;
			annoncesL = Manager.getInstance().listerAnnonceL(codePostal, check1, check2, check3, check4);
		}
		else {
			Integer surfaceint = Integer.parseInt(surface);
			annoncesL = Manager.getInstance().listerAnnonceL(codePostal, surfaceint, check1, check2, check3, check4);
		}
		request.setAttribute("annonces", annoncesL);
	
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/annoncesLocataire.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String codePostal = request.getParameter("codepostal");
		String surface = request.getParameter("surface");

		String check1 = request.getParameter("checkbox1");
		String check2 = request.getParameter("checkbox2");
		String check3 = request.getParameter("checkbox3");
		String check4 = request.getParameter("checkbox4");
		
		
		response.sendRedirect("annoncesLocataire?codP="+codePostal+"&surf="+surface+"&check1="+check1+"&check2="+check2+"&check3="+check3+"&check4="+check4);
	}

}
