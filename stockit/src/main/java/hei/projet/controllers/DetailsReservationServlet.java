package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceP;
import hei.projet.model.Photo;
import hei.projet.model.Reservation;
import hei.projet.model.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DetailsReservationServlet
 */
@WebServlet("/DetailsReservationServlet")
public class DetailsReservationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailsReservationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Integer idReservation = Integer.parseInt(request.getParameter("idReservation"));
		Reservation reservation = Manager.getInstance().getReservation(idReservation);
		AnnonceP annonceP = Manager.getInstance().getAnnonceP(reservation.getId_annp());
		List<Photo> photos = Manager.getInstance().getPhotoAnnonceP(annonceP.getId_annp());
		ArrayList<List> liste = Manager.getInstance().listeNoteUser(annonceP.getId_user());
		ArrayList<List> listeL = Manager.getInstance().listeNoteUser(reservation.getId_user());
		Integer nombrePhoto = photos.size();
		if(nombrePhoto<4){
			for(int i=0;i<4-nombrePhoto;i++){
				Photo photo = new Photo(null,"fondGris.jpg");
				photos.add(photo);
			}
		}
		User proprietaire = Manager.getInstance().getInformationUser(annonceP.getId_user());
		User locataire = Manager.getInstance().getInformationUser(reservation.getId_user());
		
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		if(Manager.getInstance().getInformationUser(userConnecte.getId_user()).getId_user()==reservation.getId_user()){
			request.setAttribute("telephone", proprietaire);
		}else{
			request.setAttribute("telephone", locataire);
		}
		
		request.setAttribute("reservation", reservation);
		request.setAttribute("annonceP", annonceP);
		request.setAttribute("photos", photos);
		request.setAttribute("proprietaire", proprietaire);
		request.setAttribute("locataire", locataire);
		request.setAttribute("notes", liste.get(0));
		request.setAttribute("noteurs",liste.get(1));
		request.setAttribute("notesL", listeL.get(0));
		request.setAttribute("noteursL",listeL.get(1));
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/detailsReservation.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
