package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.NoteUser;
import hei.projet.model.User;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MonProfil
 */
@WebServlet("/MonProfilServlet")
public class MonProfilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MonProfilServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		
		User userInformations = Manager.getInstance().getInformationUser(userConnecte.getId_user());
		
		List<NoteUser> notes = Manager.getInstance().listerNote(userConnecte.getId_user());
		double sommeNote = 0;
		double note = 0;
		DecimalFormat df = new DecimalFormat ( ) ; 
		df.setMaximumFractionDigits ( 1 ) ; 
		df.setMinimumFractionDigits ( 0 ) ; 
		df.setDecimalSeparatorAlwaysShown (false) ;
		if(notes.size() != 0){
			for(int i=0;i<notes.size()-1;i++){
				sommeNote = sommeNote + notes.get(i).getId_note();
			}
			note = sommeNote/notes.size();
			String not = df.format(note);
			request.setAttribute("note", not);
		}else{
			request.setAttribute("note","Pas encore noté");
		}
		
		request.setAttribute("userInformations", userInformations);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/monProfil.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
