package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceL;
import hei.projet.model.TypeEspace;
import hei.projet.model.User;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ModifierAnnonceLocataireServlet
 */
@WebServlet("/ModifierAnnonceLocataireServlet")
public class ModifierAnnonceLocataireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private Integer idAnnl;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifierAnnonceLocataireServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		idAnnl = Integer.parseInt(request.getParameter("idAnnL"));
		AnnonceL annonceL = Manager.getInstance().getAnnonceL(idAnnl);
		request.setAttribute("annonceL", annonceL);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/modifierAnnonceLocataire.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		int nombreErreur=0;
		List<TypeEspace> listeTypeEspace = new ArrayList<TypeEspace>();
		String codePostal = request.getParameter("codePostalL");
		String description = request.getParameter("descriptionL");
		String ville = request.getParameter("villeL");
		Integer surface = null;
	    try {
	    	surface = Integer.parseInt(request.getParameter("surfaceL"));
		} catch (NumberFormatException e) {
			request.setAttribute("surfaceL", "Vous devez saisir une valeur numérique." );
		}
	    String periodeLocation = request.getParameter("periodeLocationL");
	    Integer prix=null;
	    
	    try{
	    	if(periodeLocation.compareTo("hebdomadaire") == 0){
		    	try {
		    		prix = Integer.parseInt(request.getParameter("prixHebdomadaireL"));
				} catch (NumberFormatException e) {
					request.setAttribute("prixHebdomadaire", "Vous devez saisir une valeur numérique." );
					nombreErreur++;
				}
		    }
		    if(periodeLocation.compareTo("mensuel") == 0){
		    	try {
		    		prix = Integer.parseInt(request.getParameter("prixMensuelL"));
				} catch (NumberFormatException e) {
					request.setAttribute("prixMensuel", "Vous devez saisir une valeur numérique." );
					nombreErreur++;
				}
		    }
	    }catch(NullPointerException e){
	    	nombreErreur++;
	    }
	    
	    Date dateDebut = null;
		try {
			dateDebut = dateFormat.parse(request.getParameter("datedebut"));
		} catch (ParseException e) {
			nombreErreur=nombreErreur+1;
		}
		Date dateFin = null;
		try {
			dateFin = dateFormat.parse(request.getParameter("datefin"));
		} catch (ParseException e) {
			nombreErreur=nombreErreur+1;
		}
		for(int i=1;i<5;i++){
			String checkbox = request.getParameter("checkbox"+i);
			if(checkbox != null){
				TypeEspace typeEspace = new TypeEspace(null,checkbox);
				listeTypeEspace.add(typeEspace);
			}
		}
		if(nombreErreur==0){
			AnnonceL annonceL = new AnnonceL(idAnnl,null,surface,periodeLocation, dateDebut,dateFin,prix,description,ville,codePostal,userConnecte.getId_user());
			Manager.getInstance().modifierAnnonceLL(annonceL, listeTypeEspace);
			response.sendRedirect("mesAnnonces");
		}else{
			response.sendRedirect("modifierAnnonceLocataire?idAnnL="+idAnnl);
		}
	}

}
