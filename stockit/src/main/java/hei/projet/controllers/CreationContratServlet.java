package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceP;
import hei.projet.model.Reservation;
import hei.projet.model.User;

import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * Servlet implementation class CreationContratServlet
 */
@WebServlet("/CreationContratServlet")
public class CreationContratServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreationContratServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    private static final Font CATFONT = new Font(Font.getFamily("TIMES_ROMAN"), 18, Font.BOLD);
    private static final Font REDFONT = new Font(Font.getFamily("TIMES_ROMAN"), 12, Font.NORMAL);
    private static final Font SMALLFONT = new Font(Font.getFamily("TIMES_ROMAN"), 10, Font.NORMAL);
    private static final Font SUBFONT = new Font(Font.getFamily("TIMES_ROMAN"), 16, Font.BOLD);
    private static final Font SMALLBOLD = new Font(Font.getFamily("TIMES_ROMAN"), 12, Font.BOLD);

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Integer idReservation = Integer.parseInt(request.getParameter("idReservation"));
		Reservation reservation = Manager.getInstance().getReservation(idReservation);
		AnnonceP annonce = Manager.getInstance().getAnnonceP(reservation.getId_annp());
		User loueur = Manager.getInstance().getInformationUser(annonce.getId_user());
		User locataire = Manager.getInstance().getInformationUser(reservation.getId_user());
		String nomFichier = "contrat"+idReservation+".pdf";
		
		Document document = new Document(PageSize.A4);
		try {
			PdfWriter.getInstance(document,new FileOutputStream("/users/tristandhumieres/git/projet-iti/stockit/src/main/webapp/contrat/"+nomFichier));
			document.open();
			Paragraph espace1 = new Paragraph();
			ajouterLigneVide(espace1, 2);
			Paragraph espace2 = new Paragraph();
			ajouterLigneVide(espace1, 1);
			Paragraph titre = new Paragraph("Contrat de mise à disposition d'espace",CATFONT);
			titre.setAlignment(Element.ALIGN_CENTER);
			document.add(titre);
			document.add(espace2);
			Paragraph sousTitre1 = new Paragraph("Loueur :",SUBFONT);
			document.add(sousTitre1);
			document.add(espace2);
			Paragraph nom = new Paragraph("Nom / Prenom :"+loueur.getNom()+" "+loueur.getPrenom(),REDFONT);
			document.add(nom);
			Paragraph dateN = new Paragraph("Née le :"+loueur.getDateNaissance(),REDFONT);
			document.add(dateN);
			Paragraph telephone = new Paragraph("Téléphone :"+loueur.getNumTel(),REDFONT);
			document.add(telephone);
			Paragraph adresse = new Paragraph("Adresse :"+loueur.getAdresse()+" "+loueur.getCodePostal()+" "+loueur.getVille(),REDFONT);
			document.add(adresse);
			document.add(espace2);
			Paragraph sousTitre2 = new Paragraph("Locataire :",SUBFONT);
			document.add(sousTitre2);
			Paragraph nom1 = new Paragraph("Nom / Prenom :"+locataire.getNom()+" "+locataire.getPrenom(),REDFONT);
			Paragraph dateN1 = new Paragraph("Née le :"+locataire.getDateNaissance(),REDFONT);
			document.add(dateN1);
			Paragraph telephone1 = new Paragraph("Téléphone :"+locataire.getNumTel(),REDFONT);
			document.add(telephone1);
			Paragraph adresse1 = new Paragraph("Adresse :"+locataire.getAdresse()+" "+locataire.getCodePostal()+" "+locataire.getVille(),REDFONT);
			document.add(adresse1);
			Paragraph sousTitre3 = new Paragraph("Caractéristique de l'espace :",SUBFONT);
			document.add(sousTitre3);
			Paragraph type = new Paragraph("Type d'espace :"+annonce.getTypeEspace(),REDFONT);
			document.add(type);
			Paragraph superficie = new Paragraph("Superficie :"+annonce.getSurface(),REDFONT);
			document.add(superficie);
			Paragraph adresseEspace = new Paragraph("Adresse de l'espace :"+annonce.getAdresse()+" "+annonce.getCodePostal()+" "+annonce.getVille(),REDFONT);
			document.add(adresseEspace);
			Paragraph conditions = new Paragraph("Conditions supplémentaires :",REDFONT);
			document.add(conditions);
			document.add(espace1);
			Paragraph sousTitre4 = new Paragraph("Détails de la location :",SUBFONT);
			document.add(sousTitre4);
			Paragraph dates = new Paragraph("Du :"+reservation.getDateDebut()+" au "+reservation.getDateFin(),REDFONT);
			document.add(dates);
			Paragraph prix = new Paragraph("Prix :"+annonce.getPrix()+"€/"+annonce.getPeriodeLocation(),REDFONT);
			document.add(prix);
			Paragraph sousTitre5 = new Paragraph("Attention :",SUBFONT);
			document.add(sousTitre5);
			Paragraph item1 = new Paragraph("-Les deux parties au contrat doivent être des particuliers ou une personne morale duement représentée :",SMALLFONT);
			document.add(item1);
			Paragraph item2 = new Paragraph("-Les deux parties au contrat doivent être agés d'au moins 18 ans.",SMALLFONT);
			document.add(item2);
			Paragraph item3 = new Paragraph("-Les matériaux dangereux, toxiques, illicites,périssables, et les organismes vivants sont interdits.",SMALLFONT);
			document.add(item3);
			document.add(espace1);
			Paragraph item4 = new Paragraph("Je soussigné(e) ............................., ai examiné attentivement l'état de l'espace et confirme qu'il es fidèlement décrit ci-dessus. Je prends la responsabilité de cet espace et m'engage à le restituer dans l'état iitial à la date indiquée sur ce contrat.", SMALLFONT);
			item4.setAlignment(Element.ALIGN_JUSTIFIED);
			document.add(item4);
			document.add(espace1);
			Paragraph signature =new Paragraph("Signature du propriétaire de l'espace :",SUBFONT);
			document.add(signature);
			document.add(espace1);
			Paragraph signature1 =new Paragraph("Signature de l'occupant de l'espace :",SUBFONT);
			document.add(signature1);
			
		} catch (DocumentException de) {
			de.printStackTrace();
			} catch (IOException ioe) {
			ioe.printStackTrace();
		}
			document.close();
			
	response.sendRedirect("contrat/"+nomFichier);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	private void ajouterLigneVide(Paragraph paragraph, int number) {
	    for (int i = 0; i < number; i++) {
	        paragraph.add(new Paragraph(" "));
	    }
	}
	
}
