package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceL;
import hei.projet.model.AnnonceP;
import hei.projet.model.TypeEspace;
import hei.projet.model.User;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DetailsAnnonceLocataireServlet
 */
@WebServlet("/DetailsAnnonceLocataireServlet")
public class DetailsAnnonceLocataireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailsAnnonceLocataireServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		
		Integer idAnnonceL =Integer.parseInt(request.getParameter("idAnnL"));
		AnnonceL annonceL = Manager.getInstance().getAnnonceL(idAnnonceL);
		User annonceur = Manager.getInstance().getInformationUser(annonceL.getId_user());
		List<TypeEspace> liste = Manager.getInstance().getTypeEspaces(idAnnonceL);
		List<AnnonceP> listeAnnonceP = Manager.getInstance().listerAnnonceP(userConnecte.getId_user());
		
		request.setAttribute("annonceL", annonceL);
		request.setAttribute("annonceur", annonceur);
		request.setAttribute("typeEspace", liste);
		request.setAttribute("listeAnnonceP", listeAnnonceP);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/detailsAnnonceLocataire.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
