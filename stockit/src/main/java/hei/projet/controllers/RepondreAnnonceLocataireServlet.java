package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceL;
import hei.projet.model.AnnonceP;
import hei.projet.model.Reservation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RepondreAnnonceLocataireServlet
 */
@WebServlet("/RepondreAnnonceLocataireServlet")
public class RepondreAnnonceLocataireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RepondreAnnonceLocataireServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Integer idLocataire = Integer.parseInt(request.getParameter("idLocataire"));
		Integer idAnnL = Integer.parseInt(request.getParameter("idAnnL"));
		Integer idAnnP = Integer.parseInt(request.getParameter("idAnnP"));
		AnnonceL annonceL = Manager.getInstance().getAnnonceL(idAnnL);
		AnnonceP annonceP = Manager.getInstance().getAnnonceP(idAnnP);
		
		Reservation reservation = new Reservation(null, "En attente de paiement", null, annonceL.getDateDebut(), annonceL.getDateFin(), annonceP.getPeriodeLocation(), annonceP.getPrix(), "", idAnnP, idLocataire);
		Manager.getInstance().creerReservation(reservation);
		Manager.getInstance().rendreInvisibleAnnonceP(idAnnP);
		
		response.sendRedirect("mesLocations");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
