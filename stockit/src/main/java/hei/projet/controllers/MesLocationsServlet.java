package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceP;
import hei.projet.model.Photo;
import hei.projet.model.Reservation;
import hei.projet.model.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MesLocationsServlet
 */
@WebServlet("/MesLocationsServlet")
public class MesLocationsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MesLocationsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		
		List<Reservation> listeReservation = Manager.getInstance().listerReservation(userConnecte.getId_user());
		request.setAttribute("listeReservation", listeReservation);
		
		List<AnnonceP> listeAnnonceP = new ArrayList<AnnonceP>();
		List<Photo> listePhoto = new ArrayList<Photo>();
		List<User> listeAnnonceur = new ArrayList<User>();
		if(listeReservation.size()!=0){
			for(int i=0;i<listeReservation.size();i++){
			AnnonceP annonceP = Manager.getInstance().getAnnonceP(listeReservation.get(i).getId_annp());
			listeAnnonceP.add(annonceP);
			Photo photo = Manager.getInstance().getPhotoUnique(annonceP.getId_annp());
			listePhoto.add(photo);
			User annonceur = Manager.getInstance().getInformationUser(annonceP.getId_user());
			listeAnnonceur.add(annonceur);
			}
			request.setAttribute("listeAnnonceP", listeAnnonceP);
			request.setAttribute("listePhoto", listePhoto);
			request.setAttribute("listeAnnonceur", listeAnnonceur);
		}
		
		List<Reservation> listeReservationEspacePropose = Manager.getInstance().listerReservationEspacePropose(userConnecte.getId_user());
		request.setAttribute("listeReservationEspacePropose", listeReservationEspacePropose);
		
		List<AnnonceP> listeAnnoncePropose = new ArrayList<AnnonceP>();
		List<Photo> listePhotoAnnoncePropose = new ArrayList<Photo>();
		List<User> listeLocataire = new ArrayList<User>();
		if(listeReservationEspacePropose.size()!=0){
			for(int i=0;i<listeReservationEspacePropose.size();i++){
				AnnonceP annonceP = Manager.getInstance().getAnnonceP(listeReservationEspacePropose.get(i).getId_annp());
				listeAnnoncePropose.add(annonceP);
				Photo photo = Manager.getInstance().getPhotoUnique(annonceP.getId_annp());
				listePhotoAnnoncePropose.add(photo);
				User locataire = Manager.getInstance().getInformationUser(listeReservationEspacePropose.get(i).getId_user());
				listeLocataire.add(locataire);
			}
			request.setAttribute("listeAnnoncePropose", listeAnnoncePropose);
			request.setAttribute("listePhotoAnnoncePropose", listePhotoAnnoncePropose);
			request.setAttribute("listeLocataire", listeLocataire);
		}
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/mesLocations.jsp");
		view.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
