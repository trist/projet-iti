package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceP;
import hei.projet.model.Photo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AnnoncesProprietaireServlet
 */
@WebServlet("/AnnoncesProprietaireServlet")
public class AnnoncesProprietaireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnnoncesProprietaireServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<Photo> listePhoto = new ArrayList<Photo>();
		String codePostal = request.getParameter("codP");
		String depLoc = request.getParameter("depLoc");
		Boolean deploc;

		
		String surface = request.getParameter("surf");
		String check1 = request.getParameter("check1");
		String check2 = request.getParameter("check2");
		String check3 = request.getParameter("check3");
		String check4 = request.getParameter("check4");
		List<AnnonceP> annoncesP = new ArrayList<AnnonceP>();
		annoncesP = Manager.getInstance().listerAnnonceP();
		if (codePostal==null && depLoc==null && surface==null && check1==null && check2==null && check3==null && check4==null) {
			annoncesP = Manager.getInstance().listerAnnonceP();
		}
		else if (depLoc.compareTo("")==0 && surface.compareTo("")==0 && (check1.compareTo("cave")!=0 && check2.compareTo("grenier")!=0 && check3.compareTo("garage")!=0 && check4.compareTo("piece a vivre")!=0)){
			annoncesP = Manager.getInstance().listerAnnonceP(codePostal);
		}
		else if (surface.compareTo("")==0 && (check1.compareTo("cave")!=0 && check2.compareTo("grenier")!=0 && check3.compareTo("garage")!=0 && check4.compareTo("piece a vivre")!=0)){
			if (depLoc.compareTo("0")==1){
				deploc=false;
			}
			else {
				deploc=true;
			}
			annoncesP = Manager.getInstance().listerAnnonceP(codePostal, deploc);
		}
		else if (depLoc.compareTo("")==0 && (check1.compareTo("cave")!=0 && check2.compareTo("grenier")!=0 && check3.compareTo("garage")!=0 && check4.compareTo("piece a vivre")!=0)) {
			Integer surfaceint = Integer.parseInt(surface);
			annoncesP = Manager.getInstance().listerAnnonceP(codePostal, surfaceint);
		}
		else if (depLoc.compareTo("")==0 && surface.compareTo("")==0){
			annoncesP = Manager.getInstance().listerAnnonceP(codePostal, check1, check2, check3, check4);
		}
		else if (depLoc.compareTo("")==0){
			Integer surfaceint = Integer.parseInt(surface);
			annoncesP = Manager.getInstance().listerAnnonceP(codePostal, surfaceint, check1, check2, check3, check4);
		}
		else if (surface.compareTo("")==0){
			if (depLoc.compareTo("0")==1){
				deploc=false;
			}
			else {
				deploc=true;
			}
			annoncesP = Manager.getInstance().listerAnnonceP(codePostal, deploc, check1, check2, check3, check4);
			
		}
		else if (check1.compareTo("cave")!=0 && check2.compareTo("grenier")!=0 && check3.compareTo("garage")!=0 && check4.compareTo("piece a vivre")!=0){
			if (depLoc.compareTo("0")==1){
				deploc=false;
			}
			else {
				deploc=true;
			}
			Integer surfaceint = Integer.parseInt(surface);
			annoncesP = Manager.getInstance().listerAnnonceP(codePostal, deploc, surfaceint);
		}
		else {
			if (depLoc.compareTo("0")==1){
				deploc=false;
			}
			else {
				deploc=true;
			}
			Integer surfaceint = Integer.parseInt(surface);
			annoncesP = Manager.getInstance().listerAnnonceP(codePostal, deploc, surfaceint, check1, check2, check3, check4);
		}
		for (int i=0;annoncesP.size()>i;i++){
			listePhoto.add(Manager.getInstance().getPhotoUnique(annoncesP.get(i).getId_annp()));
		}
		request.setAttribute("listePhoto", listePhoto);

		request.setAttribute("annonces", annoncesP);
		
	
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/annoncesProprietaire.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String codePostal = request.getParameter("codepostal");
		String depLoc = request.getParameter("type");
		String surface = request.getParameter("surface");

		String check1 = request.getParameter("checkbox1");
		String check2 = request.getParameter("checkbox2");
		String check3 = request.getParameter("checkbox3");
		String check4 = request.getParameter("checkbox4");
		
		
		response.sendRedirect("annoncesProprietaire?codP="+codePostal+"&depLoc="+depLoc+"&surf="+surface+"&check1="+check1+"&check2="+check2+"&check3="+check3+"&check4="+check4);
	}

}
