package hei.projet.controllers;

import hei.projet.metier.Manager;
import hei.projet.model.AnnonceP;
import hei.projet.model.Photo;
import hei.projet.model.Reservation;
import hei.projet.model.User;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DetailsAnnonceServlet
 */
@WebServlet("/DetailsAnnonceServlet")
public class DetailsAnnonceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private String periodePaiement;
	private Integer prix;
	private Integer idAnnp;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailsAnnonceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		Integer idAnnonceP = Integer.parseInt(request.getParameter("idAnnP"));
		AnnonceP annonceP = Manager.getInstance().getAnnonceP(idAnnonceP);
		User proprietaire = Manager.getInstance().getInformationUser(annonceP.getId_user());
		ArrayList<List> liste = Manager.getInstance().listeNoteUser(annonceP.getId_user());
		periodePaiement = annonceP.getPeriodeLocation();
		prix = annonceP.getPrix();
		idAnnp = annonceP.getId_annp();
		request.setAttribute("annonceP", annonceP);
		request.setAttribute("proprietaire", proprietaire);
		request.setAttribute("notes", liste.get(0));
		request.setAttribute("noteurs",liste.get(1));
		
		Boolean depotLocation = annonceP.isDep_Loc();
		if(depotLocation == false){
			request.setAttribute("depotLocation", "Dépot");
		}else{
			if(depotLocation == true){
				request.setAttribute("depotLocation", "Location");
			}
		}
		
		List<Photo> photos = Manager.getInstance().getPhotoAnnonceP(idAnnonceP);
		Integer nombrePhoto = photos.size();
		if(nombrePhoto<4){
			for(int i=0;i<4-nombrePhoto;i++){
				Photo photo = new Photo(null,"fondGris.jpg");
				photos.add(photo);
			}
		}
		request.setAttribute("photos", photos);
		
		RequestDispatcher view = request.getRequestDispatcher("WEB-INF/pages/detailsAnnonceProprietaire.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession(true);
		User userConnecte = (User) session.getAttribute("utilisateurConnecte");
		
		Integer idUser = userConnecte.getId_user();
		
		Date dateDebut = null;
		try {
			dateDebut = dateFormat.parse(request.getParameter("dateDebut"));
		} catch (ParseException e) {
		}
		Date dateFin = null;
		try {
			dateFin = dateFormat.parse(request.getParameter("dateFin"));
		} catch (ParseException e) {
		}
		
		String description = request.getParameter("description");
		
		if(dateDebut != null && dateFin !=null){
			Reservation reservation = new Reservation(null, "En attente de validation", null, dateDebut, dateFin, periodePaiement, prix, description, idAnnp, idUser);
			Manager.getInstance().creerReservation(reservation);
			Manager.getInstance().rendreInvisibleAnnonceP(idAnnp);
			response.sendRedirect("mesLocations");
		}else{
			response.sendRedirect("detailsAnnonceProprietaire?idAnnP="+idAnnp);
		}
		
	}

}
