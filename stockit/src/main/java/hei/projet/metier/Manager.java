package hei.projet.metier;



import java.util.ArrayList;
import java.util.List;












import hei.projet.dao.AnnonceLDao;
import hei.projet.dao.AnnoncePDao;
import hei.projet.dao.MessageDao;
import hei.projet.dao.NoteUserDao;
import hei.projet.dao.ReservationDao;
import hei.projet.dao.UserDao;
import hei.projet.model.AnnonceL;
import hei.projet.model.AnnonceP;
import hei.projet.model.Message;
import hei.projet.model.NoteUser;
import hei.projet.model.Photo;
import hei.projet.model.Reservation;
import hei.projet.model.TypeEspace;
import hei.projet.model.User;



public class Manager {
	
	private static Manager instance;
	
	private UserDao userDao;
	private AnnoncePDao annoncePDao;
	private AnnonceLDao annonceLDao;
	private ReservationDao reservationDao;
	private NoteUserDao noteUserDao;
	private MessageDao messageDao;
	
	public static Manager getInstance() {
		if(instance == null) {
			instance = new Manager();
		}
		return instance;
	}
	
	
	private Manager() {
		userDao = new UserDao();
		annoncePDao = new AnnoncePDao();
		annonceLDao = new AnnonceLDao();
		reservationDao = new ReservationDao();
		noteUserDao = new NoteUserDao();
		messageDao = new MessageDao();
	}
	
	public int ajouterUtilisateur(User user) {
		return userDao.ajouterUtilisateur(user);
	}
	
	public boolean EmailExiste(String email){
		return userDao.EmailExiste(email);
	}
	
	public boolean UtilisateurExiste(User user){
		return userDao.UtilisateurExiste(user);
	}

	public User getUser(String email){
		return userDao.GetUser(email);
	}
	
	public User getInformationUser(int idUser){
		return userDao.GetInformationUser(idUser);
	}
	
	public void modifierUser(User user){
		userDao.modifierUser(user);
	}
	
	public void ajouterAnnonceP(AnnonceP annonceP, List<Photo> liste){
		annoncePDao.ajouterAnnonceP(annonceP, liste);
	}
	
	public void ajouterAnnonceL(AnnonceL annonceL, List<TypeEspace> liste){
		annonceLDao.ajouterAnnonceL(annonceL,liste);
	}
	
	public void supprimerAnnonceP(Integer idAnnonceP){
		annoncePDao.supprimerAnnonceP(idAnnonceP);
	}
	
	public void modifierAnnonceP(AnnonceP annonceP, List<Photo> liste){
		annoncePDao.modifierAnnonceP(annonceP, liste);
	}
	
	public AnnonceP getAnnonceP(Integer idAnnonceP){
		AnnonceP annonceP =annoncePDao.getAnnonceP(idAnnonceP);
		return annonceP;
	}
	
	public void rendreInvisibleAnnonceP(Integer idAnnoncP){
		annoncePDao.rendreInvisibleAnnonceP(idAnnoncP);
	}
	
	public void rendreVisibleAnnonceP(Integer idAnnoncP){
		annoncePDao.rendreVisibleAnnonceP(idAnnoncP);
	}
	
	public List<Photo> getPhotoAnnonceP(Integer idAnnonceP){
		List<Photo> liste = annoncePDao.getPhotoAnnonceP(idAnnonceP);
		return liste;
	}
	
	public Photo getPhotoUnique(Integer idAnnonceP){
		Photo photo = annoncePDao.getPhotoUnique(idAnnonceP);
		return photo;
	}
	
	public List<AnnonceP> listerAnnonceP() {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP();
		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal) {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP(codePostal);
		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal, Boolean depLoc) {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP(codePostal, depLoc);
		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal, Integer surface) {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP(codePostal, surface);
		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP(codePostal, checkbox1, checkbox2, checkbox3, checkbox4);
		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal,Boolean depLoc, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP(codePostal, depLoc, checkbox1, checkbox2, checkbox3, checkbox4);
		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal,Integer surface, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP(codePostal, surface, checkbox1, checkbox2, checkbox3, checkbox4);
		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal,Boolean depLoc, Integer surface) {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP(codePostal, depLoc, surface);
		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal,Boolean depLoc, Integer surface, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP(codePostal, depLoc, surface, checkbox1, checkbox2, checkbox3, checkbox4);
		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(Integer id_user) {
		List<AnnonceP> liste = annoncePDao.listerAnnonceP(id_user);
		return liste;
	}

	public AnnonceL getAnnonceL(Integer idAnnonceL){
		AnnonceL annonceL =annonceLDao.getAnnonceL(idAnnonceL);
		return annonceL;
	}
	
	public List<TypeEspace> getTypeEspaces(Integer idAnnonceL){
		List<TypeEspace> liste = annonceLDao.getTypeEspace(idAnnonceL);
		return liste;
	}
	
	
	public List<AnnonceL> listerAnnonceL() {
		List<AnnonceL> liste = annonceLDao.listerAnnonceL();
		return liste;
	}
	
	public List<AnnonceL> listerAnnonceL(String codePostal) {
		List<AnnonceL> liste = annonceLDao.listerAnnonceL(codePostal);
		return liste;
	}
	
	
	public List<AnnonceL> listerAnnonceL(String codePostal, Integer surface) {
		List<AnnonceL> liste = annonceLDao.listerAnnonceL(codePostal, surface);
		return liste;
	}
	
	public List<AnnonceL> listerAnnonceL(String codePostal, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceL> liste = annonceLDao.listerAnnonceL(codePostal, checkbox1, checkbox2, checkbox3, checkbox4);
		return liste;
	}
	
	
	
	public List<AnnonceL> listerAnnonceL(String codePostal,Integer surface, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceL> liste = annonceLDao.listerAnnonceL(codePostal, surface, checkbox1, checkbox2, checkbox3, checkbox4);
		return liste;
	}
	
	public List<AnnonceL> listerAnnonceL(Integer idUser) {
		List<AnnonceL> liste = annonceLDao.listerAnnonceL(idUser);
		return liste;
	}
	
	public void modifierAnnonceLL(AnnonceL annonceL, List<TypeEspace> liste){
		annonceLDao.modifierAnnonceL(annonceL, liste);
	}
	
	public void supprimerAnnonceL(Integer idAnnonceL){
		annonceLDao.supprimerAnnonceL(idAnnonceL);
	}
	
	
	public void creerReservation(Reservation reservation){
		reservationDao.creerReservation(reservation);
	}
	
	public List<Reservation> listerReservation(Integer idUser){
		List<Reservation> listeReservation = reservationDao.listerReservation(idUser);
		return listeReservation;
	}
	
	public List<Reservation> listerReservationEspacePropose(Integer idUser){
		List<Reservation> listeReservationEspacePropose = reservationDao.listerReservationEspacePropose(idUser);
		return listeReservationEspacePropose;
	}
	
	public void changerStatutReservation(Integer idReservation, String statut){
		reservationDao.changerStatutReservation(idReservation, statut);
	}
	
	public void annulerReservation(Integer idReservation){
		reservationDao.annulerReservation(idReservation);
	}
	
	public Reservation getReservation(Integer idReservation){
		return reservationDao.getReservation(idReservation);
	}
	
	public List<User> listerUser(){
		List <User> listeUser = userDao.ListerUser();
		return listeUser;
	}
	
	public void supprimerUser(Integer idUser){
		userDao.supprimerUser(idUser);
	}

	public void promouvoirUser(Integer idUser){
		userDao.promouvoirAdminUser(idUser);
	}
	
	public void degraderUser(Integer idUser){
		userDao.degraderUser(idUser);
	}
	
	public List<User> listerUserAdmin(Integer idUser){
		List<User> listeUserAdmin = userDao.ListerUserAdmin(idUser);
		return listeUserAdmin;
	}
	
	public void ajouterNote(NoteUser note){
		noteUserDao.ajouterNote(note);
	}
	
	public ArrayList<List> listeNoteUser(Integer idUser){
		ArrayList<List> liste = noteUserDao.listerNoteUser(idUser);
		return liste;
	}
	
	public void envoyerMessage(Message message){
		messageDao.envoyerMessage(message);
	}
	
	public List<List> listerMessage(Integer id_User){
		List<List> liste = messageDao.listerMessage(id_User);
		return liste;
	}
	
	public List<NoteUser> listerNote(Integer idUser){
		List<NoteUser> listeNote = noteUserDao.listerNote(idUser);
		return listeNote;
	}
	
}
