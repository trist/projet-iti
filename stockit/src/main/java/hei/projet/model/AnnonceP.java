package hei.projet.model;


import java.util.Date;

public class AnnonceP {
	private Integer id_annp;
	private Date datePubl;
	private boolean dep_Loc;
	private String periodeLocation;
	private String typeEspace;
	private Integer surface;
	private String adresse;
	private Integer prix;
	private String description;
	private String ville;
	private String codePostal;
	private Boolean visibilite;
	private Integer id_user;
	
	public AnnonceP(Integer id, Date dateP, Boolean depLoc,String periodeLoc, String typeEsp, Integer surface, String adresse, Integer prix, String descrip, String ville, String codPost,Boolean visibilite , Integer idU){
		this.id_annp=id;
		this.datePubl=dateP;
		this.dep_Loc=depLoc;
		this.periodeLocation=periodeLoc;
		this.typeEspace=typeEsp;
		this.surface=surface;
		this.adresse=adresse;
		this.prix=prix;
		this.description=descrip;
		this.ville=ville;
		this.codePostal=codPost;
		this.visibilite=visibilite;
		this.id_user=idU;
		
	}

	public Boolean getVisibilite() {
		return visibilite;
	}

	public void setVisibilite(Boolean visibilite) {
		this.visibilite = visibilite;
	}

	public String getPeriodeLocation() {
		return periodeLocation;
	}

	public void setPeriodeLocation(String periodeLocation) {
		this.periodeLocation = periodeLocation;
	}

	public Integer getId_annp() {
		return id_annp;
	}

	public void setId_annp(Integer id_annp) {
		this.id_annp = id_annp;
	}

	public Date getDatePubl() {
		return datePubl;
	}

	public void setDatePubl(Date datePubl) {
		this.datePubl = datePubl;
	}

	public boolean isDep_Loc() {
		return dep_Loc;
	}

	public void setDep_Loc(boolean dep_Loc) {
		this.dep_Loc = dep_Loc;
	}

	public String getTypeEspace() {
		return typeEspace;
	}

	public void setTypeEspace(String typeEspace) {
		this.typeEspace = typeEspace;
	}

	public Integer getSurface() {
		return surface;
	}

	public void setSurface(Integer surface) {
		this.surface = surface;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}



	public Integer getPrix() {
		return prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId_user() {
		return id_user;
	}

	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
}