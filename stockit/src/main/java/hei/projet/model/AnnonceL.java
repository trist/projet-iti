package hei.projet.model;


import java.util.Date;

public class AnnonceL {
	private Integer id_annl;
	private Date datePubl;
	private Integer surface;
	private String periodeLoc;
	private Date dateDebut;
	private Date dateFin;
	private Integer prix;
	private String description;
	private String ville;
	private String codePostal;
	private Integer id_user;
	
	public AnnonceL(Integer id, Date dateP, Integer surface, String perLoc,  Date dateD, Date dateF, Integer prix, String descrip, String ville, String codPost, Integer idUser){
		this.id_annl=id;
		this.datePubl=dateP;
		this.surface=surface;
		this.periodeLoc=perLoc;
		this.dateDebut=dateD;
		this.dateFin=dateF;
		this.prix=prix;
		this.description=descrip;
		this.ville=ville;
		this.codePostal=codPost;
		this.id_user=idUser;
		
	}
	
	public Integer getId_user() {
		return id_user;
	}

	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}

	public String getPeriodeLoc() {
		return periodeLoc;
	}

	public void setPeriodeLoc(String periodeLoc) {
		this.periodeLoc = periodeLoc;
	}

	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public Integer getId_annl() {
		return id_annl;
	}
	public void setId_annl(Integer id_annl) {
		this.id_annl = id_annl;
	}
	public Date getDatePubl() {
		return datePubl;
	}
	public void setDatePubl(Date datePubl) {
		this.datePubl = datePubl;
	}

	public Integer getSurface() {
		return surface;
	}
	public void setSurface(Integer surface) {
		this.surface = surface;
	}
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public Integer getPrix() {
		return prix;
	}
	public void setPrix(Integer prix) {
		this.prix = prix;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
