package hei.projet.model;

public class TypeEspace {
	private Integer id_typeEspace;
	private String typeEspace;
	
	public TypeEspace(Integer id, String typeE){
		this.id_typeEspace=id;
		this.typeEspace=typeE;
	}

	public Integer getId_typeEspace() {
		return id_typeEspace;
	}

	public void setId_typeEspace(Integer id_typeEspace) {
		this.id_typeEspace = id_typeEspace;
	}

	public String getTypeEspace() {
		return typeEspace;
	}

	public void setTypeEspace(String typeEspace) {
		this.typeEspace = typeEspace;
	}

}
