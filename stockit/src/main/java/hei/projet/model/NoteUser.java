package hei.projet.model;

public class NoteUser {
	private Integer id_note;
	private Integer note;
	private String commentaire;
	private Integer id_User;
	private Integer id_UserNote;
	
	public NoteUser(Integer id, Integer note, String comm, Integer id_User, Integer id_UserNote){
		this.id_note=id;
		this.note=note;
		this.commentaire=comm;
		this.id_User=id_User;
		this.id_UserNote=id_UserNote;
	}

	public Integer getId_note() {
		return id_note;
	}

	public void setId_note(Integer id_note) {
		this.id_note = id_note;
	}

	public Integer getNote() {
		return note;
	}

	public void setNote(Integer note) {
		this.note = note;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Integer getId_User() {
		return id_User;
	}

	public void setId_User(Integer id_User) {
		this.id_User = id_User;
	}

	public Integer getId_UserNote() {
		return id_UserNote;
	}

	public void setId_UserNote(Integer id_UserNote) {
		this.id_UserNote = id_UserNote;
	}
	
}
