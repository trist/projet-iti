package hei.projet.model;

public class Message {
	private Integer id_message;
	private String sujet;
	private String contenu;
	private Integer id_emetteur;
	private Integer id_destinataire;
	
	public Message (Integer id,String sujet, String contenu, Integer id_emetteur, Integer id_destinataire){
		this.id_message=id;
		this.sujet=sujet;
		this.contenu=contenu;
		this.id_emetteur=id_emetteur;
		this.id_destinataire=id_destinataire;
	}

	public Integer getId_message() {
		return id_message;
	}

	public void setId_message(Integer id_message) {
		this.id_message = id_message;
	}

	public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Integer getId_emetteur() {
		return id_emetteur;
	}

	public void setId_emetteur(Integer id_emetteur) {
		this.id_emetteur = id_emetteur;
	}

	public Integer getId_destinataire() {
		return id_destinataire;
	}

	public void setId_destinataire(Integer id_destinataire) {
		this.id_destinataire = id_destinataire;
	}
}
