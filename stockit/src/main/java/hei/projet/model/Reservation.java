package hei.projet.model;

import java.util.Date;

public class Reservation {
	private Integer id_reserv;
	private String statut;
	private String contrat;
	private Date dateDebut;
	private Date dateFin;
	private String periodePaiement;
	private Integer prix;
	private String description;
	private Integer id_annp;
	private Integer id_user;
	
	public Reservation(Integer id, String statut,String contrat, Date dateD, Date dateF,String periodePaiement, Integer prix,String description, Integer id_annp, Integer id_user){
		this.id_reserv=id;
		this.statut=statut;
		this.contrat=contrat;
		this.dateDebut=dateD;
		this.dateFin=dateF;
		this.periodePaiement=periodePaiement;
		this.prix=prix;
		this.description=description;
		this.id_annp=id_annp;
		this.id_user=id_user;
	}

	public Integer getId_reserv() {
		return id_reserv;
	}

	public void setId_reserv(Integer id_reserv) {
		this.id_reserv = id_reserv;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getContrat() {
		return contrat;
	}

	public void setContrat(String contrat) {
		this.contrat = contrat;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	
	public String getPeriodePaiement() {
		return periodePaiement;
	}

	public void setPeriodePaiement(String periodePaiement) {
		this.periodePaiement = periodePaiement;
	}
	
	public Integer getPrix() {
		return prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getId_annp() {
		return id_annp;
	}

	public void setId_annp(Integer id_annp) {
		this.id_annp = id_annp;
	}
	
	public Integer getId_user() {
		return id_user;
	}

	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}
}
