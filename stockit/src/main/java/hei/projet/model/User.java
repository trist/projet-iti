package hei.projet.model;

import java.util.Date;

public class User {
	private Integer id_user;
	private String nom;
	private String prenom;
	private String mail;
	private String password;
	private Date dateNaissance;
	private String numTel;
	private boolean visibiliteTel;
	private String adresse;
	private String ville;
	private String codePostal;
	private String photo;
	private boolean admin;
	
	
	public User (Integer id,String nom,String prenom,String email,String psswd,Date dateN,String num, Boolean visi,String adress,String ville, String codePostal, String photo,Boolean admin){
		this.id_user=id;
		this.nom=nom;
		this.prenom=prenom;
		this.mail=email;
		this.password=psswd;
		this.dateNaissance=dateN;
		this.numTel=num;
		this.visibiliteTel=visi;
		this.adresse=adress;
		this.ville=ville;
		this.codePostal=codePostal;
		this.photo=photo;
		this.admin=admin;
	}
	
	public User(Integer id, String nom, String prenom, String email, Date dateN, String numTel, Boolean visibiliteTel, String adresse, String ville, String codePostal, String photo){
		this.id_user=id;
		this.nom=nom;
		this.prenom=prenom;
		this.mail=email;
		this.dateNaissance=dateN;
		this.numTel=numTel;
		this.visibiliteTel=visibiliteTel;
		this.adresse=adresse;
		this.ville=ville;
		this.codePostal=codePostal;
		this.photo=photo;
	}
	
	public User (Integer id,String nom,String prenom,String email,String psswd){
		this.id_user=id;
		this.nom=nom;
		this.prenom=prenom;
		this.mail=email;
		this.password=psswd;
	}
	

	
	public User (String email,String psswd){
		this.mail=email;
		this.password=psswd;
	}
	
	public Integer getId_user() {
		return id_user;
	}


	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Date getDateNaissance() {
		return dateNaissance;
	}


	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}


	public String getNumTel() {
		return numTel;
	}


	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}


	public boolean isVisibiliteTel() {
		return visibiliteTel;
	}


	public void setVisibiliteTel(boolean visibiliteTel) {
		this.visibiliteTel = visibiliteTel;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}
	
	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}
	
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}



	public boolean isAdmin() {
		return admin;
	}


	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	
}
