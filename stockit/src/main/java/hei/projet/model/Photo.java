package hei.projet.model;

public class Photo {
	private Integer id_photo;
	private String photo;
	
	
	public Photo(Integer id, String photo){
		this.id_photo=id;
		this.photo=photo;
	}


	public Integer getId_photo() {
		return id_photo;
	}


	public void setId_photo(Integer id_photo) {
		this.id_photo = id_photo;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
