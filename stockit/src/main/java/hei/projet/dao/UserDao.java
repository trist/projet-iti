package hei.projet.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import hei.projet.model.User;

public class UserDao {

	
	public int ajouterUtilisateur(User user) {
		int idUser = 0;
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("INSERT INTO `user`(`nom`,`prenom`,`email`,`password`,`dateNaissance`,`numeroTel`,`visibiliteTel`,`adresse`,`ville`,`codePostal`,`photo`,`admin`) VALUES(?, ?, ?, ?,?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, user.getNom());
			stmt.setString(2, user.getPrenom());
			stmt.setString(3, user.getMail());
			stmt.setString(4, user.getPassword());
			stmt.setDate(5,null);
			stmt.setString(6, "");
			stmt.setBoolean(7, false);
			stmt.setString(8, "");
			stmt.setString(9,"");
			stmt.setString(10,"");
			stmt.setString(11,"fondGris.jpg");
			stmt.setBoolean(12,false);
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next()) idUser = rs.getInt(1);
			stmt.close();
			
			// Fermer la connexion
			
			connection.close();
			
			

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return idUser;
	}
	
	public void modifierUser(User user) {
		
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("UPDATE user SET nom = ?, prenom = ?, email = ?, dateNaissance = ?,numeroTel = ?, visibiliteTel = ?, adresse = ?, ville = ?, codePostal = ?, photo = ? WHERE id_User = ? ");
			stmt.setString(1, user.getNom());
			stmt.setString(2, user.getPrenom());
			stmt.setString(3, user.getMail());
			stmt.setDate(4, new Date(user.getDateNaissance().getTime()));
			stmt.setString(5, user.getNumTel());
			stmt.setBoolean(6, user.isVisibiliteTel());
			stmt.setString(7, user.getAdresse());
			stmt.setString(8, user.getVille());
			stmt.setString(9, user.getCodePostal());
			stmt.setString(10, user.getPhoto());
			stmt.setInt(11, user.getId_user());
			stmt.executeUpdate();

			// Fermer la connexion
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean EmailExiste(String email){
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
	
			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email = ?");
			stmt.setString(1, email);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				return false; 
			}
			stmt.close();
			connection.close();
			
		} catch (SQLException e) {
		e.printStackTrace();
		}
		return true;
		
	}
	
	public boolean UtilisateurExiste(User user){
	try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email = ?");
			stmt.setString(1, user.getMail());
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				return true; 
			}
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public User GetUser(String email){
		User user = null;
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM user WHERE email = ?");
			stmt.setString(1, email);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				user = new User(result.getInt("id_user"),
					result.getString("nom"),result.getString("prenom"),
					result.getString("email"),result.getString("password"),
					result.getDate("dateNaissance"),result.getString("numeroTel"),
					result.getBoolean("visibiliteTel"),result.getString("adresse"),result.getString("ville"),result.getString("codePostal"),result.getString("photo"),
					result.getBoolean("admin"));
				
			}
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public User GetInformationUser(int idUser){
		User user = null;
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM user WHERE id_User = ?");
			stmt.setInt(1, idUser);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				user = new User(result.getInt("id_user"),
					result.getString("nom"),result.getString("prenom"),
					result.getString("email"),result.getString("password"),
					result.getDate("dateNaissance"),result.getString("numeroTel"),
					result.getBoolean("visibiliteTel"),result.getString("adresse"),result.getString("ville"),result.getString("codePostal"),result.getString("photo"),
					result.getBoolean("admin"));
				
			}
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public List<User> ListerUser(){
		List<User> listeUser = new ArrayList<User>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM user WHERE admin = 0");
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				User user = new User(results.getInt("id_user"),
					results.getString("nom"),results.getString("prenom"),
					results.getString("email"),results.getString("password"),
					results.getDate("dateNaissance"),results.getString("numeroTel"),
					results.getBoolean("visibiliteTel"),results.getString("adresse"),results.getString("ville"),results.getString("codePostal"),results.getString("photo"),
					results.getBoolean("admin"));
					listeUser.add(user);
				
			}
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeUser;
	}
	
	public List<User> ListerUserAdmin(Integer idUser){
		List<User> listeUser = new ArrayList<User>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM user WHERE admin = 1 AND id_user <> ?");
			stmt.setInt(1, idUser);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				User user = new User(results.getInt("id_user"),
					results.getString("nom"),results.getString("prenom"),
					results.getString("email"),results.getString("password"),
					results.getDate("dateNaissance"),results.getString("numeroTel"),
					results.getBoolean("visibiliteTel"),results.getString("adresse"),results.getString("ville"),results.getString("codePostal"),results.getString("photo"),
					results.getBoolean("admin"));
					listeUser.add(user);
				
			}
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeUser;
	}
	
	public void supprimerUser(Integer idUser){
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM user WHERE id_user = ?");
			stmt.setInt(1, idUser);
			stmt.executeUpdate();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void promouvoirAdminUser(Integer idUser){
		
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("UPDATE user SET admin = 1 WHERE id_user = ?");
			stmt.setInt(1, idUser);
			stmt.executeUpdate();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
public void degraderUser(Integer idUser){
		
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("UPDATE user SET admin = 0 WHERE id_user = ?");
			stmt.setInt(1, idUser);
			stmt.executeUpdate();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
