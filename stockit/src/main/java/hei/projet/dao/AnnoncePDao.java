package hei.projet.dao;


import hei.projet.model.Photo;
import hei.projet.model.AnnonceP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AnnoncePDao {
	
	public List<AnnonceP> listerAnnonceP() {
		List<AnnonceP> liste = new ArrayList<AnnonceP>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM annonceP WHERE visibilite=1 ORDER BY datePubl DESC");
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}

			
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	
	public List<AnnonceP> listerAnnonceP(String codePostal) {
		ArrayList<AnnonceP> liste = new ArrayList<AnnonceP>();
		String departement = codePostal.substring(0,2);
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT * FROM annonceP WHERE (codePostal=? OR codePostal LIKE ?) AND visibilite=1  ORDER BY codePostal=? DESC ");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setString(3, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}

			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	
	public List<AnnonceP> listerAnnonceP(String codePostal, Boolean depLoc) {
		List<AnnonceP> liste = new ArrayList<AnnonceP>();
		Integer deploc;
		if (depLoc){
			deploc=1;
		}
		else {
			deploc=0;
		}
		String departement = codePostal.substring(0,2);
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM annonceP WHERE (codePostal=? OR codePostal LIKE ? )AND dep_loc=?  AND visibilite=1 ORDER BY codePostal=? DESC ");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setInt(3, deploc);
			stmt.setString(4, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}

			
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

			
		return liste;
	}
	
	
	
	
	public List<AnnonceP> listerAnnonceP(String codePostal, Integer surface) {
		List<AnnonceP> liste = new ArrayList<AnnonceP>();
		String departement = codePostal.substring(0,2);
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM annonceP WHERE  (codePostal=? OR codePostal LIKE ? ) AND annonceP.surface>=? AND visibilite=1  ORDER BY codePostal=? DESC ");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setInt(3, surface);
			stmt.setString(4, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}

			
			results.close();
			stmt.close();

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	
	
	public List<AnnonceP> listerAnnonceP(String codePostal, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceP> liste = new ArrayList<AnnonceP>();
		String departement = codePostal.substring(0,2);
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM annonceP WHERE  (codePostal=? OR codePostal LIKE ? ) AND (annonceP.typeEspace=? OR annonceP.typeEspace=? OR annonceP.typeEspace=? OR annonceP.typeEspace=?)  AND visibilite=1 ORDER BY codePostal=? DESC ");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setString(3, checkbox1);
			stmt.setString(4, checkbox2);
			stmt.setString(5, checkbox3);
			stmt.setString(6, checkbox4);
			stmt.setString(7, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}

			
			results.close();
			stmt.close();

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	
	
	public List<AnnonceP> listerAnnonceP(String codePostal,Boolean depLoc, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceP> liste = new ArrayList<AnnonceP>();
		Integer deploc;
		if (depLoc){
			deploc=1;
		}
		else {
			deploc=0;
		}
		String departement = codePostal.substring(0,2);
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM annonceP WHERE  (codePostal=? OR codePostal LIKE ? ) AND annonceP.dep_loc=? AND (annonceP.typeEspace=? OR annonceP.typeEspace=? OR annonceP.typeEspace=? OR annonceP.typeEspace=?)  AND visibilite=1 ORDER BY codePostal=? DESC ");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setInt(3, deploc);
			stmt.setString(4, checkbox1);
			stmt.setString(5, checkbox2);
			stmt.setString(6, checkbox3);
			stmt.setString(7, checkbox4);
			stmt.setString(8, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}

			
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}


		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal,Integer surface, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceP> liste = new ArrayList<AnnonceP>();
		String departement = codePostal.substring(0,2);
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM annonceP WHERE  (codePostal=? OR codePostal LIKE ? ) AND annonceP.surface>=? AND (annonceP.typeEspace=? OR annonceP.typeEspace=? OR annonceP.typeEspace=? OR annonceP.typeEspace=?) AND visibilite=1  ORDER BY codePostal=? DESC ");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setInt(3, surface);
			stmt.setString(4, checkbox1);
			stmt.setString(5, checkbox2);
			stmt.setString(6, checkbox3);
			stmt.setString(7, checkbox4);
			stmt.setString(8, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}

			
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		

		return liste;
	}
	
	public List<AnnonceP> listerAnnonceP(String codePostal,Boolean depLoc, Integer surface) {
		List<AnnonceP> liste = new ArrayList<AnnonceP>();
		String departement = codePostal.substring(0,2);
		Integer deploc;
		if (depLoc){
			deploc=1;
		}
		else {
			deploc=0;
		}
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM annonceP WHERE  (codePostal=? OR codePostal LIKE ? ) AND annonceP.dep_loc=? AND annonceP.surface>=?  AND visibilite=1 ORDER BY codePostal=? DESC ");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setInt(3, deploc);
			stmt.setInt(4, surface);
			stmt.setString(5, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}


			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	    
		return liste;
	}
	
	
	public List<AnnonceP> listerAnnonceP(String codePostal,Boolean depLoc, Integer surface, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceP> liste = new ArrayList<AnnonceP>();
		String departement = codePostal.substring(0,2);
		Integer deploc;
		if (depLoc){
			deploc=1;
		}
		else {
			deploc=0;
		}
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM annonceP WHERE  (codePostal=? OR codePoste LIKE ? ) AND dep_loc=? AND annonceP.surface>=? AND (annonceP.typeEspace=? OR annonceP.typeEspace=? OR annonceP.typeEspace=? OR annonceP.typeEspace=?)  AND visibilite=1 ORDER BY codePostal=? DESC ");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setInt(3, deploc);
			stmt.setInt(4, surface);
			stmt.setString(5, checkbox1);
			stmt.setString(6, checkbox2);
			stmt.setString(7, checkbox3);
			stmt.setString(8, checkbox4);
			stmt.setString(9, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}

			
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	
	
	public List<AnnonceP> listerAnnonceP(Integer idUser) {
		ArrayList<AnnonceP> liste = new ArrayList<AnnonceP>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM annonceP WHERE id_user=? ORDER BY datePubl DESC ");
			
			stmt.setInt(1, idUser);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceP annonceP = new AnnonceP(results.getInt("id_annp"),
						results.getDate("datePubl"),
						results.getBoolean("dep_loc"), results.getString("periodeLoc"),
						results.getString("typeEspace"),
						results.getInt("surface"), results.getString("adresse"),
						results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getBoolean("visibilite"),results.getInt("id_user"));
				liste.add(annonceP);
			}
	
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	public void ajouterAnnonceP(AnnonceP annonceP, List<Photo> liste) {
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			PreparedStatement stmt = connection
					.prepareStatement("INSERT INTO `annonceP`(`datePubl`,`dep_loc`,`periodeLoc`,`typeEspace`,`surface`,`adresse`,`prix`,`description`,`ville`,`codePostal`,`id_user`) VALUES(NOW(), ?, ?, ?, ?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			stmt.setBoolean(1, annonceP.isDep_Loc());
			stmt.setString(2, annonceP.getPeriodeLocation());
			stmt.setString(3, annonceP.getTypeEspace());
			stmt.setInt(4, annonceP.getSurface());
			stmt.setString(5, annonceP.getAdresse());
			stmt.setInt(6, annonceP.getPrix());
			stmt.setString(7, annonceP.getDescription());
			stmt.setString(8, annonceP.getVille());
			stmt.setString(9, annonceP.getCodePostal());
			stmt.setInt(10, annonceP.getId_user());
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			int idDerniereAnnonceInserer = 0;
			if(rs.next()) idDerniereAnnonceInserer = rs.getInt(1);
			
			int nombrePhoto = liste.size();
			
			if(nombrePhoto>=1){
				for (int i=0;i<nombrePhoto;i++){
					PreparedStatement stmt2 = connection.prepareStatement("INSERT INTO `photo`(`photo`,`id_annp`) VALUES(?, ?)");
					stmt2.setString(1, liste.get(i).getPhoto());
					stmt2.setInt(2, idDerniereAnnonceInserer);
					stmt2.executeUpdate();
					stmt.close();
					stmt2.close();
				}
			}
			else {
					PreparedStatement stmt2 = connection.prepareStatement("INSERT INTO `photo`(`id_annp`) VALUES(?)");
					stmt2.setInt(1, idDerniereAnnonceInserer);
					stmt2.executeUpdate();
					stmt.close();
					stmt2.close();
				}
			// Fermer la connexion

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
	
	public void supprimerAnnonceP(Integer idAnnonceP) {
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("DELETE FROM `annonceP` WHERE `id_annp`=?");
			stmt.setInt(1, idAnnonceP);
			
			PreparedStatement stmt2 = connection.prepareStatement("DELETE FROM `photo` WHERE `id_annp`=?");
			stmt2.setInt(1, idAnnonceP);
			stmt2.executeUpdate();
			stmt.executeUpdate();
			

			// Fermer la connexion
			stmt.close();
			stmt2.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public AnnonceP getAnnonceP(Integer idAnnonceP) {
		AnnonceP annonceP = null;
		
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM annonceP WHERE id_annp = ?");
			stmt.setInt(1, idAnnonceP);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				annonceP = new AnnonceP(result.getInt("id_annp"),
						result.getDate("datePubl"),
						result.getBoolean("dep_loc"), result.getString("periodeLoc"),
						result.getString("typeEspace"),
						result.getInt("surface"), result.getString("adresse"),
						result.getInt("prix"), result.getString("description"),
						result.getString("ville"), result.getString("codePostal"),result.getBoolean("visibilite"),result.getInt("id_user"));
			}
			
			
			
			// Fermer la connexion
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return annonceP;
	}
	
	
	public  List<Photo> getPhotoAnnonceP(Integer idAnnonceP) {
		List<Photo> listePhoto = new ArrayList<Photo>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
		PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM photo WHERE id_annp = ?");
		stmt.setInt(1, idAnnonceP);
		ResultSet results = stmt.executeQuery();
		while (results.next()) {
				Photo photo = new Photo(results.getInt("id_photo"),
						results.getString("photo"));

				listePhoto.add(photo);
			}
		stmt.close();
		connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listePhoto;
	}
	
	
	public  Photo getPhotoUnique(Integer idAnnonceP) {
		Photo photo = null;
		try {
			
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
		PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM photo WHERE id_annp = ? ORDER BY id_photo DESC");
		stmt.setInt(1, idAnnonceP);
		ResultSet results = stmt.executeQuery();
		while (results.next()) {
				photo = new Photo(results.getInt("id_photo"),
						results.getString("photo"));
			}
		stmt.close();
		connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return photo;
	}
	
	public void modifierAnnonceP(AnnonceP annonceP,List<Photo> liste) {
		
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("UPDATE annonceP SET dep_Loc = ?,  periodeLoc= ?, typeEspace = ?, surface = ?, adresse = ?, prix = ?, description = ?, ville = ?, codePostal = ? WHERE id_annp = ? ");
			stmt.setBoolean(1, annonceP.isDep_Loc());
			stmt.setString(2, annonceP.getPeriodeLocation());
			stmt.setString(3, annonceP.getTypeEspace());
			stmt.setInt(4, annonceP.getSurface());
			stmt.setString(5, annonceP.getAdresse());
			stmt.setInt(6, annonceP.getPrix());
			stmt.setString(7, annonceP.getDescription());
			stmt.setString(8, annonceP.getVille());
			stmt.setString(9, annonceP.getCodePostal());
			stmt.setInt(10, annonceP.getId_annp());
			stmt.executeUpdate();
			
			int nombrePhoto = liste.size();
			
			if(nombrePhoto>=1){
				
				PreparedStatement stmt1 = connection.prepareStatement("DELETE FROM photo WHERE id_annp = ?");
				stmt1.setInt(1, annonceP.getId_annp());
				stmt1.executeUpdate();
				stmt1.close();
				
				for (int i=0;i<nombrePhoto;i++){
					PreparedStatement stmt2 = connection.prepareStatement("INSERT INTO `photo`(`photo`,`id_annp`) VALUES(?, ?)");
					stmt2.setString(1, liste.get(i).getPhoto());
					stmt2.setInt(2, annonceP.getId_annp());
					stmt2.executeUpdate();
					stmt.close();
					stmt2.close();
					}
			}

			// Fermer la connexion
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void rendreVisibleAnnonceP(Integer idAnnoncP){
		
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("UPDATE annonceP SET visibilite=1 WHERE id_annp = ? ");
			stmt.setInt(1, idAnnoncP);
			stmt.executeUpdate();
			stmt.close();
			
		}catch (SQLException e){
			e.printStackTrace();
		}
		
	}
	
public void rendreInvisibleAnnonceP(Integer idAnnoncP){
		
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("UPDATE annonceP SET visibilite=0 WHERE id_annp = ? ");
			stmt.setInt(1, idAnnoncP);
			stmt.executeUpdate();
			stmt.close();
			
		}catch (SQLException e){
			e.printStackTrace();
		}
		
	}
		
}
	
