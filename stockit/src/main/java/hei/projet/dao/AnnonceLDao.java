package hei.projet.dao;



import hei.projet.model.AnnonceL;
import hei.projet.model.TypeEspace;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AnnonceLDao {
	
	public List<AnnonceL> listerAnnonceL() {
		List<AnnonceL> liste = new ArrayList<AnnonceL>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT annonceL.* FROM annonceL ORDER BY annonceL.datePubl DESC");
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceL annonceL = new AnnonceL(results.getInt("id_annl"),
						results.getDate("datePubl"),
						results.getInt("surface"), results.getString("periodeLoc"),results.getDate("dateDebut"), 
						results.getDate("dateFin"), results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getInt("id_user"));
				liste.add(annonceL);
			}
			

			// Fermer la connexion
			results.close();
			stmt.close();
			
			
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	
	public List<AnnonceL> listerAnnonceL(String codePostal) {
		List<AnnonceL> liste = new ArrayList<AnnonceL>();
		String departement = codePostal.substring(0,2);
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT annonceL.* FROM annonceL WHERE annonceL.codePostal=? OR annonceL.codePostal LIKE ? ORDER BY annonceL.codePostal=? DESC");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setString(3, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceL annonceL = new AnnonceL(results.getInt("id_annl"),
						results.getDate("datePubl"),
						results.getInt("surface"),results.getString("periodeLoc"), results.getDate("dateDebut"), 
						results.getDate("dateFin"), results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getInt("id_user"));
				liste.add(annonceL);
			}
			

			// Fermer la connexion
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	
	
	
	public List<AnnonceL> listerAnnonceL(String codePostal, Integer surface) {
		List<AnnonceL> liste = new ArrayList<AnnonceL>();
		String departement = codePostal.substring(0,2);

		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT annonceL.* FROM annonceL WHERE (annonceL.codePostal=? OR annonceL.codePostal LIKE ?) AND annonceL.surface<=? ORDER BY annonceL.codePostal=? DESC");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setInt(3, surface);
			stmt.setString(4, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceL annonceL = new AnnonceL(results.getInt("id_annl"),
						results.getDate("datePubl"),
						results.getInt("surface"),results.getString("periodeLoc"), results.getDate("dateDebut"), 
						results.getDate("dateFin"), results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getInt("id_user"));
				liste.add(annonceL);
			}

			// Fermer la connexion
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	public List<AnnonceL> listerAnnonceL(String codePostal, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceL> liste = new ArrayList<AnnonceL>();
		String departement = codePostal.substring(0,2);
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT annonceL.* FROM annonceL INNER JOIN typeEspace ON annonceL.id_annl=typeEspace.id_annl WHERE (annonceL.codePostal=? OR annonceL.codePostal LIKE ?) AND (typeEspace.typeEspace=? OR typeEspace.typeEspace=? OR typeEspace.typeEspace=? OR typeEspace.typeEspace=?) ORDER BY annonceL.codePostal=? DESC");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setString(3, checkbox1);
			stmt.setString(4, checkbox2);
			stmt.setString(5, checkbox3);
			stmt.setString(6, checkbox4);
			stmt.setString(7, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceL annonceL = new AnnonceL(results.getInt("id_annl"),
						results.getDate("datePubl"),
						results.getInt("surface"),results.getString("periodeLoc"), results.getDate("dateDebut"), 
						results.getDate("dateFin"), results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getInt("id_user"));
				liste.add(annonceL);
			}

			// Fermer la connexion
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	
	
	public List<AnnonceL> listerAnnonceL(String codePostal,Integer surface, String checkbox1, String checkbox2, String checkbox3, String checkbox4) {
		List<AnnonceL> liste = new ArrayList<AnnonceL>();
		String departement = codePostal.substring(0,2);
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT annonceL.* FROM annonceL INNER JOIN typeEspace ON annonceL.id_annl=typeEspace.id_annl WHERE (annonceL.codePostal=? OR annonceL.codePostal LIKE ?) AND annonceL.surface<=? AND (typeEspace.typeEspace=? OR typeEspace.typeEspace=? OR typeEspace.typeEspace=? OR typeEspace.typeEspace=?) ORDER BY annonceL.codePostal=? DESC");
			
			stmt.setString(1, codePostal);
			stmt.setString(2, departement+"%");
			stmt.setInt(3, surface);
			stmt.setString(4, checkbox1);
			stmt.setString(5, checkbox2);
			stmt.setString(6, checkbox3);
			stmt.setString(7, checkbox4);
			stmt.setString(8, codePostal);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceL annonceL = new AnnonceL(results.getInt("id_annl"),
						results.getDate("datePubl"),
						results.getInt("surface"),results.getString("periodeLoc"), results.getDate("dateDebut"), 
						results.getDate("dateFin"), results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getInt("id_user"));
				liste.add(annonceL);
			}

			// Fermer la connexion
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	

	public List<AnnonceL> listerAnnonceL(Integer idUser) {
		List<AnnonceL> liste = new ArrayList<AnnonceL>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT annonceL.* FROM annonceL WHERE annonceL.id_user=? ORDER BY annonceL.datePubl DESC");
			
			stmt.setInt(1, idUser);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				AnnonceL annonceL = new AnnonceL(results.getInt("id_annl"),
						results.getDate("datePubl"),
						results.getInt("surface"),results.getString("periodeLoc"), results.getDate("dateDebut"), 
						results.getDate("dateFin"), results.getInt("prix"), results.getString("description"),
						results.getString("ville"), results.getString("codePostal"),results.getInt("id_user"));
				liste.add(annonceL);
			}
			

			// Fermer la connexion
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	public void ajouterAnnonceL(AnnonceL annonceL,List<TypeEspace> liste) {
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("INSERT INTO `annonceL`(`datePubl`,`surface`,`periodeLoc`,`dateDebut`,`dateFin`,`prix`,`description`,`ville`,`codePostal`,`id_user`) VALUES(NOW(), ?, ?, ?, ?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, annonceL.getSurface());
			stmt.setString(2, annonceL.getPeriodeLoc());
			stmt.setDate(3, new Date(annonceL.getDateDebut().getTime()));
			stmt.setDate(4, new Date(annonceL.getDateFin().getTime()));
			stmt.setInt(5, annonceL.getPrix());
			stmt.setString(6, annonceL.getDescription());
			stmt.setString(7, annonceL.getVille());
			stmt.setString(8, annonceL.getCodePostal());
			stmt.setInt(9, annonceL.getId_user());
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			int idDerniereAnnonceInserer = 0;
			if(rs.next()) idDerniereAnnonceInserer = rs.getInt(1);
			stmt.close();
			int nombreTypeEspace = liste.size();
			
			if(nombreTypeEspace>=1){
				for (int i=0;i<nombreTypeEspace;i++){
					PreparedStatement stmt2 = connection.prepareStatement("INSERT INTO `typeEspace`(`typeEspace`,`id_annl`) VALUES(?, ?)");
					stmt2.setString(1, liste.get(i).getTypeEspace());
					stmt2.setInt(2, idDerniereAnnonceInserer);
					stmt2.executeUpdate();
					stmt.close();
					stmt2.close();
				}
			}
			
			// Fermer la connexion

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void supprimerAnnonceL(Integer idAnnonceL) {
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt1 = connection
					.prepareStatement("DELETE FROM typeEspace WHERE `id_annl`=?");
			stmt1.setInt(1, idAnnonceL);
			stmt1.executeUpdate();
			stmt1.close();
			
			PreparedStatement stmt = connection
					.prepareStatement("DELETE FROM `annonceL` WHERE `id_annl`=?");
			stmt.setInt(1, idAnnonceL);
			stmt.executeUpdate();
			

			// Fermer la connexion
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public AnnonceL getAnnonceL(Integer idAnnonceL) {
		AnnonceL annonceL = null;
		
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM annonceL WHERE id_annl = ?");
			stmt.setInt(1, idAnnonceL);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				annonceL = new AnnonceL(result.getInt("id_annl"),
						result.getDate("datePubl"),
						result.getInt("surface"), result.getString("periodeLoc"), result.getDate("dateDebut"), 
						result.getDate("dateFin"), result.getInt("prix"), result.getString("description"),
						result.getString("ville"), result.getString("codePostal"),result.getInt("id_user"));
			}
			
			// Fermer la connexion
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return annonceL;
	}
	
	public List<TypeEspace> getTypeEspace(Integer idAnnonceL){
		List<TypeEspace> liste = new ArrayList<TypeEspace>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection
					.prepareStatement("SELECT * FROM typeEspace WHERE id_annl = ?");
			stmt.setInt(1, idAnnonceL);
			ResultSet result = stmt.executeQuery();
			while (result.next()) {
				TypeEspace type = new TypeEspace(result.getInt("id_type"),result.getString("typeEspace"));
				liste.add(type);
			}
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
			return liste;
		}
	
public void modifierAnnonceL(AnnonceL annonceL,List<TypeEspace> liste) {
		
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("UPDATE annonceL SET surface = ?,  periodeLoc= ?, dateDebut = ?, dateFin = ?, prix = ?, description = ?, ville = ?, codePostal = ? WHERE id_annl = ? ");
			stmt.setInt(1, annonceL.getSurface());
			stmt.setString(2, annonceL.getPeriodeLoc());
			stmt.setDate(3, new Date(annonceL.getDateDebut().getTime()));
			stmt.setDate(4, new Date(annonceL.getDateFin().getTime()));
			stmt.setInt(5, annonceL.getPrix());
			stmt.setString(6, annonceL.getDescription());
			stmt.setString(7, annonceL.getVille());
			stmt.setString(8, annonceL.getCodePostal());
			stmt.setInt(9, annonceL.getId_annl());
			stmt.executeUpdate();
			
			int nombreType = liste.size();
			
			if(nombreType>=1){
				
				PreparedStatement stmt1 = connection.prepareStatement("DELETE FROM typeEspace WHERE id_annp = ?");
				stmt1.setInt(1, annonceL.getId_annl());
				stmt1.executeUpdate();
				stmt1.close();
				
				for (int i=0;i<nombreType;i++){
					PreparedStatement stmt2 = connection.prepareStatement("INSERT INTO `typeEspace`(`typeEspace`,`id_annl`) VALUES(?, ?)");
					stmt2.setString(1, liste.get(i).getTypeEspace());
					stmt2.setInt(2, annonceL.getId_annl());
					stmt2.executeUpdate();
					stmt.close();
					stmt2.close();
					}
			}

			// Fermer la connexion
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}