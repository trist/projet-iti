package hei.projet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hei.projet.model.NoteUser;
import hei.projet.model.User;

public class NoteUserDao {
	
	public void ajouterNote(NoteUser note){
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("INSERT INTO `note`(`note`,`commentaire`,`id_user`,`id_user_Association_11`) VALUES(?, ?, ?, ?)");
			stmt.setInt(1, note.getNote());
			stmt.setString(2, note.getCommentaire());
			stmt.setInt(3, note.getId_User());
			stmt.setInt(4, note.getId_UserNote());
			stmt.executeUpdate();
			stmt.close();
			
			// Fermer la connexion
			
			connection.close();
			

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public ArrayList<List> listerNoteUser(Integer idUser) {
		ArrayList<List> liste = new ArrayList<List>();
		ArrayList<NoteUser> listeNote = new ArrayList<NoteUser>();
		ArrayList<User> listeUser = new ArrayList<User>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM note INNER JOIN user ON note.id_user = user.id_user WHERE note.id_user_Association_11 =?");
	
			stmt.setInt(1, idUser);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				NoteUser noteUser = new NoteUser(results.getInt("id_note"),
						results.getInt("note"),
						results.getString("commentaire"), results.getInt("id_user"),
						results.getInt("id_user_Association_11"));
				listeNote.add(noteUser);
				User user = new User(null,results.getString("nom"),results.getString("prenom"),null,null,null,null,results.getBoolean("visibiliteTel"),null,null,null,null,results.getBoolean("admin"));
				listeUser.add(user);
			}
	
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		liste.add(listeNote);
		liste.add(listeUser);
		return liste;
	}
	
	public List<NoteUser> listerNote(Integer idUser) {
		
		ArrayList<NoteUser> listeNote = new ArrayList<NoteUser>();
		
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM note WHERE id_user_Association_11 =?");
	
			stmt.setInt(1, idUser);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				NoteUser noteUser = new NoteUser(results.getInt("id_note"),
						results.getInt("note"),
						results.getString("commentaire"), results.getInt("id_user"),
						results.getInt("id_user_Association_11"));
				listeNote.add(noteUser);
			}
	
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeNote;
	}

}
