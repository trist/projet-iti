package hei.projet.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hei.projet.model.Reservation;

public class ReservationDao {
	
	public void creerReservation(Reservation reservation) {

		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("INSERT INTO `reservation`(`statut`,`contrat`,`dateDebut`,`dateFin`,`periodePaiement`,`prix`,`description`,`id_annp`,`id_user`) VALUES(?, ?, ?, ?,?,?,?,?,?)");
			stmt.setString(1, reservation.getStatut());
			stmt.setString(2, "");
			stmt.setDate(3, new Date(reservation.getDateDebut().getTime()));
			stmt.setDate(4, new Date(reservation.getDateFin().getTime()));
			stmt.setString(5, reservation.getPeriodePaiement());
			stmt.setInt(6, reservation.getPrix());
			stmt.setString(7, reservation.getDescription());
			stmt.setInt(8, reservation.getId_annp());
			stmt.setInt(9, reservation.getId_user());
			stmt.executeUpdate();

			// Fermer la connexion
			
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<Reservation> listerReservation(Integer idUser) {
		ArrayList<Reservation> listeReservation = new ArrayList<Reservation>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM reservation WHERE id_user=?");
			
			stmt.setInt(1, idUser);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				Reservation reservation = new Reservation(results.getInt("id_reservation"),
						results.getString("statut"),
						results.getString("contrat"), results.getDate("dateDebut"),
						results.getDate("dateFin"),
						results.getString("periodePaiement"), results.getInt("prix"),results.getString("description"),
						results.getInt("id_annp"), results.getInt("id_user"));
				listeReservation.add(reservation);
			}
	
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listeReservation;
	}
	
	public Reservation getReservation (Integer idReservation){
		
		Reservation reservation = null;
		
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM reservation WHERE id_reservation=?");
			
			stmt.setInt(1, idReservation);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				reservation = new Reservation(result.getInt("id_reservation"),
						result.getString("statut"),
						result.getString("contrat"), result.getDate("dateDebut"),
						result.getDate("dateFin"),
						result.getString("periodePaiement"), result.getInt("prix"),result.getString("description"),
						result.getInt("id_annp"), result.getInt("id_user"));
			}
	
			result.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return reservation;
	}
	
	public List<Reservation> listerReservationEspacePropose(Integer idUser) {
		ArrayList<Reservation> listeReservation = new ArrayList<Reservation>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("SELECT reservation.* FROM reservation INNER JOIN annonceP ON reservation.id_annp = annonceP.id_annp WHERE annonceP.id_user =?");
	
			stmt.setInt(1, idUser);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				Reservation reservation = new Reservation(results.getInt("id_reservation"),
						results.getString("statut"),
						results.getString("contrat"), results.getDate("dateDebut"),
						results.getDate("dateFin"),
						results.getString("periodePaiement"), results.getInt("prix"),results.getString("description"),
						results.getInt("id_annp"), results.getInt("id_user"));
				listeReservation.add(reservation);
			}
	
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listeReservation;
	}
	
	public void changerStatutReservation(Integer idReservation, String statut){
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("UPDATE reservation SET statut = ? WHERE id_Reservation = ? ");
			stmt.setString(1, statut);
			stmt.setInt(2, idReservation);
			stmt.executeUpdate();

			// Fermer la connexion
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void annulerReservation(Integer idReservation){
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();

			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM reservation WHERE id_Reservation = ? ");
			stmt.setInt(1, idReservation);
			stmt.executeUpdate();

			// Fermer la connexion
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
