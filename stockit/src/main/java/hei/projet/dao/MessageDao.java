package hei.projet.dao;

import hei.projet.model.Message;
import hei.projet.model.NoteUser;
import hei.projet.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageDao {
	
	public void envoyerMessage(Message message){
		try {
			Connection connection = DataSourceProvider.getDataSource().getConnection();
			
			// Utiliser la connexion
			PreparedStatement stmt = connection.prepareStatement("INSERT INTO `message`(`sujet`,`message`,`id_user`,`id_user_Association_16`) VALUES(?, ?, ?, ?)");
			stmt.setString(1, message.getSujet());
			stmt.setString(2, message.getContenu());
			stmt.setInt(3, message.getId_emetteur());
			stmt.setInt(4, message.getId_destinataire());
			stmt.executeUpdate();
			stmt.close();
			
			// Fermer la connexion
			
			connection.close();
			

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<List> listerMessage(Integer id_User){
		ArrayList<List> liste = new ArrayList<List>();
		ArrayList<Message> listeMessage = new ArrayList<Message>();
		ArrayList<User> listeUser = new ArrayList<User>();
		try {
			Connection connection = DataSourceProvider.getDataSource()
					.getConnection();
			
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM message INNER JOIN user ON message.id_user = user.id_user WHERE message.id_user_Association_16 =? ORDER BY dateEnvoi DESC");
	
			stmt.setInt(1, id_User);
			ResultSet results = stmt.executeQuery();
			while (results.next()) {
				Message message = new Message(results.getInt("id_message"),
						results.getString("sujet"),
						results.getString("message"), results.getInt("id_user"),
						results.getInt("id_user_Association_16"));
				listeMessage.add(message);
				User user = new User(results.getInt("id_user"),results.getString("nom"),results.getString("prenom"),null,null,null,null,results.getBoolean("visibiliteTel"),null,null,null,null,results.getBoolean("admin"));
				listeUser.add(user);
			}
	
			results.close();
			stmt.close();
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		liste.add(listeMessage);
		liste.add(listeUser);
		return liste;
	}

}
