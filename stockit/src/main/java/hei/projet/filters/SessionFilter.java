package hei.projet.filters;

import hei.projet.model.User;

import java.io.IOException;
import java.lang.String;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SessionFilter implements Filter{
    public void init(FilterConfig filterConfig) throws ServletException {}
    public void doFilter(ServletRequest request, ServletResponse response,    FilterChain chain) throws IOException, ServletException {
       
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        
        String chemin = req.getRequestURI().substring( req.getContextPath().length() );

        if ( chemin.startsWith( "/css") || chemin.startsWith("/img") || chemin.startsWith("/js") || chemin.matches("/../img")) {
            chain.doFilter( request, response );
            return;
        }
        
        if(!req.getServletPath().equals("/connexion") && !req.getServletPath().equals("/accueil") && !req.getServletPath().equals("/annoncesLocataire") && !req.getServletPath().equals("/annoncesProprietaire") && !req.getServletPath().equals("/commentCaMarche")){
            testIfUtilisateurConnecte(req,resp,chain);
        }else{
            chain.doFilter(request, response);
        }
    }
   
   
    private void testIfUtilisateurConnecte(HttpServletRequest req,    HttpServletResponse resp,FilterChain chain) throws IOException, ServletException {
        User utilisateurConnecte = getUtilisateurConnecte(req);
        if(utilisateurConnecte==null){
            resp.sendRedirect(req.getServletContext().getContextPath()+"/connexion");
        }else{
            chain.doFilter(req, resp);
        }
    }
    private User getUtilisateurConnecte(HttpServletRequest req) {
        return (User) req.getSession().getAttribute("utilisateurConnecte");
    }
    public void destroy() {    }
}
